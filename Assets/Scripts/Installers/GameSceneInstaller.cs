using Assets.Scripts.BuildingControllers;
using Assets.Scripts.Ui.BuildingsMenu;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Installers
{
    public class GameSceneInstaller : MonoInstaller
    {
        [SerializeField] private BuildingControllers.BuildingControllers buildingControllers;
        [SerializeField] private BuildingMenu buildingMenu;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<BuildingSelectionController>().AsSingle().NonLazy();

            Container.Bind<BuildingControllers.BuildingControllers>().FromInstance(buildingControllers);
            Container.Bind<BuildingMenu>().FromInstance(buildingMenu);
        }
    }
}