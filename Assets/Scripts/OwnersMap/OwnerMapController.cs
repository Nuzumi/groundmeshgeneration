using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.OwnersMap
{
    public class OwnerMapController : MonoBehaviour
    {
        [SerializeField] private RawImage image;

        private Texture2D texture;

        private void Awake()
        {
            texture = new Texture2D(100, 100);

            for(int x = 0; x < 100; x++)
            {
                for(int y = 0; y < 100; y++)
                {
                    texture.SetPixel(x, y, new Color(1,1,1,.01f));
                }
            }

            image.texture = texture;
        }
    }
}