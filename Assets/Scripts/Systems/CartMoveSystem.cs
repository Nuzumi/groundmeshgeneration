﻿using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using Assets.Scripts.Utils;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

namespace Assets.Scripts.Systems
{

    public partial struct CartMoveSystem : ISystem
    {
        private const float MinReachedDistance = 0.02f;

        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<Cart>();
            state.RequireForUpdate<CartConfig>();
            state.RequireForUpdate<WorldGenerationConfig>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            foreach(var (cart, transform, entity) in SystemAPI.Query<Cart, LocalTransform>().WithEntityAccess())
            {
                var targetPosition = SystemAPI.GetComponent<IntrestPoint>(cart.currentTarget).position;

                if(math.distance(targetPosition, transform.Position) <= MinReachedDistance)
                {
                    SystemAPI.SetComponentEnabled<CartReachedTarget>(entity, true);
                }
            }

            var generationConfig = SystemAPI.GetSingleton<WorldGenerationConfig>();
            new CartMoveJob
            {
                intrestPointGroup = SystemAPI.GetComponentLookup<IntrestPoint>(true),
                octaveFrequencyModifiers = generationConfig.octaveFrequencies.AsReadOnly(),
                octaveAmplitudeModifiers = generationConfig.octaveAmplitudes.AsReadOnly(),
                minValue = generationConfig.minValue,
                maxValue = generationConfig.maxHeight,
                center = generationConfig.center,
                reduceEndZoneDistance = generationConfig.reduceEndZoneDistance,
                reduceStartZoneDistance = generationConfig.reduceStartZoneDistance,
                deltaTime = SystemAPI.Time.DeltaTime
            }.Schedule();
        }
    }

    [WithDisabled(typeof(CartReachedTarget))]
    [BurstCompile]
    public partial struct CartMoveJob : IJobEntity
    {
        [ReadOnly] public ComponentLookup<IntrestPoint> intrestPointGroup;
        [ReadOnly] public NativeArray<float>.ReadOnly octaveFrequencyModifiers;
        [ReadOnly] public NativeArray<float>.ReadOnly octaveAmplitudeModifiers;
        public float minValue;
        public float maxValue;
        public float2 center;
        public float reduceStartZoneDistance;
        public float reduceEndZoneDistance;
        public float deltaTime;

        public void Execute(ref LocalTransform transform, in Cart cart, CartConfig cartConfig)
        {
            var targetPosition = intrestPointGroup[cart.currentTarget].position;
            var directionToTarget = math.normalize(targetPosition - transform.Position);
            var nextPosition = transform.Position + directionToTarget * (deltaTime * cartConfig.speed);
            var nextPositionHeight = GroundCalculationsUtils.GetGroundHeight(
                octaveFrequencyModifiers,
                octaveAmplitudeModifiers,
                nextPosition.xz,
                minValue,
                maxValue, 
                center,
                reduceStartZoneDistance,
                reduceEndZoneDistance);
            nextPosition.y = nextPositionHeight;
            transform.Position = nextPosition;
        }
    }
}