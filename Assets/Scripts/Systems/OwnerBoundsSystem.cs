﻿using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using Assets.Scripts.Utils;
using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.Systems
{
    public partial struct OwnerBoundsSystem : ISystem
    {
        private const float BuildingRange = 1f;
        private const float BuildingRangeMargin = .3f;
        private const float ColorsPerUnitLength = 60;

        private static readonly Color EmptyColor = new Color(1f,1f,1f, 0);
        private static readonly Color EdgeColor = new Color(1,0,0, 1);

        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<RecalculateOwnerBounds>();
            state.RequireForUpdate<IntrestPoint>();

            state.EntityManager.CreateSingleton<OwnerBoundData>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var recaluclateConfig = SystemAPI.GetSingletonRW<RecalculateOwnerBounds>();
            if (!recaluclateConfig.ValueRO.recalculate)
            {
                return;
            }

            recaluclateConfig.ValueRW.recalculate = false;

            var neighbourOffsets = new NativeList<float3>(8, state.WorldUpdateAllocator)
            {
                new(0,0,2),
                new(2,0,2),
                new(2,0,0),
                new(2,0,-2),
                new(0,0,-2),
                new(-2,0,-2),
                new(-2,0,0),
                new(-2,0,2),
            };

            var positionsBuffer = SystemAPI.GetSingletonBuffer<RecalculateOwneerBoundsPosition>().Reinterpret<float3>();
            var data = SystemAPI.GetSingletonRW<OwnerBoundData>();

            var meshesToUpdateSet = new NativeHashSet<int>(32, Allocator.Temp);
            var meshesToUpdate = new NativeList<OwnerBoundMeshData>(Allocator.Temp);
            SetMeshDataToUpdate(neighbourOffsets, ref positionsBuffer, data, ref meshesToUpdateSet, ref meshesToUpdate);

            var positions = new NativeList<float2>(state.WorldUpdateAllocator);
            var intrestPointsColors = new NativeList<Color>(state.WorldUpdateAllocator);

            foreach (var intrestPoint in SystemAPI.Query<IntrestPoint>().WithAll<Building>())
            {
                positions.Add(intrestPoint.position.xz);
                intrestPointsColors.Add(intrestPoint.owner.ToColor());
            }

            var relevantDistanceSqr = BuildingRange * BuildingRange;

            var gridSize = meshesToUpdate[0].gridSize;
            var colorsAmount = meshesToUpdate[0].colors.Length;
            var colorsArray = new NativeArray<Color>(meshesToUpdate.Length * colorsAmount, Allocator.TempJob);

            var aabbs = new NativeArray<AABB>(meshesToUpdate.Length, Allocator.TempJob);

            for (int i = 0; i < meshesToUpdate.Length; i++)
            {
                aabbs[i] = meshesToUpdate[i].aabb;
            }

            var job = new OwnerBoundJob
            {
                aabbs = aabbs.AsReadOnly(),
                colors = colorsArray,
                colorsAmount = colorsAmount,
                gridSize = gridSize,
                intrestPointsPositions = positions.AsReadOnly(),
                intrestPointsColors = intrestPointsColors.AsReadOnly(),
                buildingSqrRangeMargin = BuildingRangeMargin * BuildingRangeMargin,
                buildingRangeMargin = BuildingRangeMargin,
                relevantDistanceSqr = relevantDistanceSqr,
                colorsPerUnitLength = ColorsPerUnitLength,
                edgeColor = EdgeColor,
                emptyColor = EmptyColor,
            }.ScheduleParallel(meshesToUpdate.Length, 1, state.Dependency);

            job.Complete();

            for (int i = 0; i < meshesToUpdate.Length; i++)
            {
                var meshData = meshesToUpdate[i];
                var startColorIndex = i * colorsAmount;

                for (int j = 0; j < colorsAmount; j++)
                {
                    meshData.colors[j] = colorsArray[startColorIndex + j];
                }

                meshData.version++;
                data.ValueRW.boundMeshDatas[meshData.id] = meshData;
            }

            aabbs.Dispose();
            colorsArray.Dispose();
            meshesToUpdate.Dispose();
            meshesToUpdateSet.Dispose();
            positionsBuffer.Clear();
        }

        private static void SetMeshDataToUpdate(NativeList<float3> neighbourOffsets, ref DynamicBuffer<float3> positionsBuffer, RefRW<OwnerBoundData> data, ref NativeHashSet<int> meshesToUpdateSet, ref NativeList<OwnerBoundMeshData> meshesToUpdate)
        {
            for (int i = 0; i < positionsBuffer.Length; i++)
            {
                var position = positionsBuffer[i];
                position.y = 0;
                var meshData = data.ValueRO.boundMeshDatas;

                for (int j = 0; j < meshData.Length; j++)
                {
                    if (meshData[j].aabb.Contains(position))
                    {
                        meshesToUpdate.Add(meshData[j]);
                        meshesToUpdateSet.Add(meshData[j].id);
                        break;
                    }
                }
            }

            for (int i = 0; i < positionsBuffer.Length; i++)
            {
                var basePosition = positionsBuffer[i];
                basePosition.y = 0;
                for (int j = 0; j < neighbourOffsets.Length; j++)
                {
                    var meshData = data.ValueRO.boundMeshDatas;
                    var position = basePosition + neighbourOffsets[j];

                    for (int k = 0; k < meshData.Length; k++)
                    {
                        if (meshesToUpdateSet.Contains(meshData[k].id))
                        {
                            continue;
                        }

                        if (meshData[k].aabb.Contains(position))
                        {
                            meshesToUpdate.Add(meshData[k]);
                            meshesToUpdateSet.Add(meshData[k].id);
                            break;
                        }
                    }
                }
            }
        }
    }

    public struct DistancePoint : IComparable<DistancePoint>
    {
        public int positionIndex;
        public float distanceSqr;
        public Color ownerColor;

        public int CompareTo(DistancePoint other) => distanceSqr.CompareTo(other.distanceSqr);
    }

    public struct OwnerBoundData : IComponentData
    {
        public NativeArray<OwnerBoundMeshData> boundMeshDatas;
    }

    public struct OwnerBoundMeshData
    {
        public int id;
        public int version;
        public NativeArray<Color> colors;
        public int2 gridSize;
        public AABB aabb;
    }

    [BurstCompile]
    public struct OwnerBoundJob : IJobFor
    {
        [NativeDisableParallelForRestriction]
        [WriteOnly]
        public NativeArray<Color> colors;
        public NativeArray<AABB>.ReadOnly aabbs;
        public NativeArray<float2>.ReadOnly intrestPointsPositions;
        public NativeArray<Color>.ReadOnly intrestPointsColors;
        public int2 gridSize;
        public float relevantDistanceSqr;
        public float buildingSqrRangeMargin;
        public float buildingRangeMargin;
        public float colorsPerUnitLength;
        public int colorsAmount;
        public Color emptyColor;
        public Color edgeColor;

        public void Execute(int index)
        {
            var startColorIndex = index * colorsAmount;
            
            for (int i = 0; i < colorsAmount; i++)
            {
                var position = GetIndexPosition(gridSize, i, aabbs[index].Min.xz);
                var closestPoints = FindRelevantClosest(position);

                colors[startColorIndex + i] = closestPoints.Length switch
                {
                    <= 0 => GetColorForNoIntrestPoints(),
                    1 => GetColorForOneIntrestPoint(closestPoints),
                    >= 2 => GetColorForManyIntrestPoints(closestPoints, position)
                };

                closestPoints.Dispose();
            }
        }

        private readonly float2 GetIndexPosition(int2 gridSize, int idx, float2 offset)
        {
            var x = idx % gridSize.x;
            var y = idx / gridSize.x;
            var position = new float2(y, x) / new float2(colorsPerUnitLength, colorsPerUnitLength);
            return position + offset;
        }

        private NativeArray<DistancePoint> FindRelevantClosest(float2 position)
        {
            var points = new NativeArray<DistancePoint>(intrestPointsPositions.Length, Allocator.Temp);

            for (int i = 0; i < points.Length; i++)
            {
                points[i] = new DistancePoint
                {
                    positionIndex = i,
                    distanceSqr = math.distancesq(position, intrestPointsPositions[i]),
                    ownerColor = intrestPointsColors[i]
                };
            }

            points.Sort();

            var result = new NativeList<DistancePoint>(Allocator.Temp);

            for (int i = 0; i < points.Length; i++)
            {
                if (points[i].distanceSqr > relevantDistanceSqr + buildingSqrRangeMargin)
                {
                    break;
                }

                result.Add(points[i]);
            }

            var resultArray = result.ToArray(Allocator.Temp);
            result.Dispose();
            return resultArray;
        }

        private readonly Color GetColorForNoIntrestPoints() => emptyColor;

        private readonly Color GetColorForOneIntrestPoint(NativeArray<DistancePoint> closestPoints)
        {
            var point = closestPoints[0];

            if (math.abs(point.distanceSqr - relevantDistanceSqr) < buildingSqrRangeMargin)
            {
                return point.ownerColor;
            }

            return emptyColor;
        }

        private readonly Color GetColorForManyIntrestPoints(NativeArray<DistancePoint> closestPoints, float2 position)
        {
            var closestPoint = closestPoints[0];
            var secondClosestPoint = closestPoints[1];

            if (closestPoint.ownerColor == secondClosestPoint.ownerColor)
            {
                if (math.abs(closestPoint.distanceSqr - relevantDistanceSqr) < buildingSqrRangeMargin
                && math.abs(secondClosestPoint.distanceSqr - relevantDistanceSqr) < buildingSqrRangeMargin)
                {
                    return closestPoint.ownerColor;
                }
            }
            else
            {
                var closestDistance = math.distance(intrestPointsPositions[closestPoint.positionIndex], position);
                var secondClosestDistance = math.distance(intrestPointsPositions[secondClosestPoint.positionIndex], position);

                if (math.abs(closestDistance - secondClosestDistance) < buildingSqrRangeMargin)
                {
                    return closestPoint.ownerColor;
                }
            }

            return emptyColor;
        }

    }
}