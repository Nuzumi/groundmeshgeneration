﻿using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using System;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Assets.Scripts.Systems
{

    public partial struct CartTargetSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<Consumer>();
            state.RequireForUpdate<Cart>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(state.WorldUnmanaged);

            new CartTargetJob
            {
                ConnectedIntrestPointGroup = SystemAPI.GetBufferLookup<ConnectedIntrestPoint>(true),
                ConsumerGroup = SystemAPI.GetBufferLookup<Consumer>(true),
                StorageGroup = SystemAPI.GetBufferLookup<Storage>(true),
                IntrestPointGroup = SystemAPI.GetComponentLookup<IntrestPoint>(true),
                commandBuffer = ecb.AsParallelWriter()
            }.ScheduleParallel();
        }
    }

    [WithAll(typeof(CartReachedTarget))]
    [WithNone(typeof(CartFinalTarget))]
    [WithNone(typeof(UnitCart))]
    [BurstCompile]
    public partial struct CartTargetJob : IJobEntity
    {
        [ReadOnly] public BufferLookup<ConnectedIntrestPoint> ConnectedIntrestPointGroup;
        [ReadOnly] public BufferLookup<Consumer> ConsumerGroup;
        [ReadOnly] public BufferLookup<Storage> StorageGroup;
        [ReadOnly] public ComponentLookup<IntrestPoint> IntrestPointGroup;

        public EntityCommandBuffer.ParallelWriter commandBuffer;

        public void Execute(ref Cart cart, in Entity entity)
        {
            var queue = new NativeQueue<Node>(Allocator.Temp);
            var routs = new NativeHashMap<Entity, Node>(50, Allocator.Temp);

            var neighbours = ConnectedIntrestPointGroup[cart.currentTarget];
            for(int i = 0; i < neighbours.Length; i++)
            {
                if (!HasSameOwner(neighbours[i].entity, cart.owner))
                {
                    continue;
                }

                var node = new Node
                {
                    value = neighbours[i].entity,
                    previous = cart.currentTarget,
                    cost = GetTravelsalCost(neighbours[i].entity, cart.currentTarget)
                };

                queue.Enqueue(node);
                routs.Add(node.value, node);
            }

            var processedEntities = new NativeHashSet<Entity>(50, Allocator.Temp);
            var possibleDestinations = new NativeList<Entity>(Allocator.Temp);

            while(queue.Count > 0)
            {
                var currentNode = queue.Dequeue();
                processedEntities.Add(currentNode.value);

                if(IsCorrectConsumer(currentNode.value, cart.product) || IsCorrectStorage(currentNode.value, cart.product))
                {
                    possibleDestinations.Add(currentNode.value);
                }

                var nodeNeighbours = ConnectedIntrestPointGroup[currentNode.value];
                for(int i = 0; i < nodeNeighbours.Length; i++)
                {
                    var neighbour = nodeNeighbours[i].entity;
                    if (processedEntities.Contains(neighbour))
                    {
                        continue;
                    }

                    if (!HasSameOwner(neighbour, cart.owner))
                    {
                        continue;
                    }

                    var node = new Node
                    {
                        value = neighbour,
                        previous = currentNode.value,
                        cost = currentNode.cost + GetTravelsalCost(neighbour, currentNode.value)
                    };

                    queue.Enqueue(node);

                    if(!routs.ContainsKey(neighbour) || routs[neighbour].cost > node.cost)
                    {
                        routs[neighbour] = node;
                    }
                }
            }

            if(possibleDestinations.Length > 0)
            {
                Entity selectedTarget = Entity.Null;

                var possibleNodeDestinations = new NativeArray<Node>(possibleDestinations.Length, Allocator.Temp);

                for(int i = 0; i < possibleDestinations.Length; i++)
                {
                    possibleNodeDestinations[i] = routs[possibleDestinations[i]];
                }

                possibleNodeDestinations.Sort();

                for(int i = 0; i < possibleNodeDestinations.Length; i++)
                {
                    if (possibleNodeDestinations[i].value.Equals(cart.finalTarget))
                    {
                        selectedTarget = cart.finalTarget;
                        break;
                    }
                }

                if (selectedTarget.Equals(Entity.Null))
                {
                    var count = math.min(4, possibleNodeDestinations.Length);
                    var threshold = 1f / count;
                    var random = Unity.Mathematics.Random.CreateFromIndex((uint)entity.Index).NextFloat();
                    for(int i = 0; i < count;i++)
                    {
                        if(random < threshold * (i + 1))
                        {
                            selectedTarget = possibleNodeDestinations[i].value;
                            break;
                        }
                    }
                }

                var tmpTarget = selectedTarget;
                while(true)
                {
                    if (routs[tmpTarget].previous.Equals(cart.currentTarget))
                    {
                        break;
                    }

                    tmpTarget = routs[tmpTarget].previous;
                }

                cart.previousTarget = cart.currentTarget;
                cart.finalTarget = selectedTarget;
                cart.currentTarget = tmpTarget;
                if (selectedTarget.Equals(tmpTarget))
                {
                    commandBuffer.SetComponentEnabled<CartFinalTarget>(entity.Index, entity, true);
                }
                commandBuffer.SetComponentEnabled<CartReachedTarget>(entity.Index, entity, false);

                possibleNodeDestinations.Dispose();
            }

            queue.Dispose();
            routs.Dispose();
            processedEntities.Dispose();
            possibleDestinations.Dispose();
        }

        private bool IsCorrectConsumer(in Entity entity, in Product product)
        {
            if (!ConsumerGroup.HasBuffer(entity))
            {
                return false;
            }

            var consumers = ConsumerGroup[entity];

            var containsProductConsumer = false;
            var consumptionRate = 0f;
            
            for(int i = 0;i < consumers.Length;i++)
            {
                if (consumers[i].product == product)
                {
                    containsProductConsumer = true;
                    consumptionRate = consumers[i].consumptionRate;
                    break;
                }
            }

            if (!containsProductConsumer)
            {
                return false;
            }

            var neededAmount = consumptionRate * 5f + 5;

            var storage = StorageGroup[entity];
            for(int i = 0; i < storage.Length; i++)
            {
                if (storage[i].product == product && storage[i].amount <= neededAmount)
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsCorrectStorage(in Entity entity, in Product product)
        {
            if (!StorageGroup.HasBuffer(entity))
            {
                return false;
            }

            var storage = StorageGroup[entity];
            for (int i = 0; i < storage.Length; i++)
            {
                if (storage[i].product == product && storage[i].amount < 0)
                {
                    return true;
                }
            }

            return false;
        }

        private float GetTravelsalCost(in Entity entity1, in Entity entity2)
        {
            var position1 = IntrestPointGroup[entity1].position;
            var position2 = IntrestPointGroup[entity2].position;

            if(math.all(position1 == position2))
            {
                return 0;
            }

            return math.distance(position1, position2);
        }

        private bool HasSameOwner(Entity entity, Owner owner)
        {
            var intrestPoint = IntrestPointGroup[entity];
            return intrestPoint.owner == owner;
        }
    }

    public struct Node : IComparable<Node>
    {
        public Entity value;
        public Entity previous;
        public float cost;

        public int CompareTo(Node other)
        {
            return cost.CompareTo(other.cost);
        }
    }
}