﻿using Assets.Scripts.BuildingControllers;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Assets.Scripts.Systems
{

    public partial struct IntrestPointConnectionSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.EntityManager.CreateSingleton<RecalculateIntrestPointConnections>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var recalculateConnections = SystemAPI.GetSingletonRW<RecalculateIntrestPointConnections>();
            if (!recalculateConnections.ValueRO.recalculate)
            {
                return;
            }

            recalculateConnections.ValueRW.recalculate = false;

            var intrestPointsQuery = SystemAPI.QueryBuilder().WithAll<IntrestPoint>().Build();
            var intrestPointsArray = intrestPointsQuery.ToComponentDataArray<IntrestPoint>(state.WorldUpdateAllocator);
            var intrestPointsEntityArray = intrestPointsQuery.ToEntityArray(state.WorldUpdateAllocator);

            new ConnectIntrestPointsJob
            {
                intrestPoints = intrestPointsArray.AsReadOnly(),
                intrestPointsEntities = intrestPointsEntityArray.AsReadOnly(),
            }.Schedule();
        }

    }

    [BurstCompile]
    public partial struct ConnectIntrestPointsJob : IJobEntity
    {
        public NativeArray<IntrestPoint>.ReadOnly intrestPoints;
        public NativeArray<Entity>.ReadOnly intrestPointsEntities;

        public void Execute(in IntrestPoint intrestPoint, ref DynamicBuffer<ConnectedIntrestPoint> connectedPoints, in Entity entity)
        {
            for(int i = 0; i < intrestPoints.Length; i++)
            {
                if (entity.Equals(intrestPointsEntities[i]))
                {
                    continue;
                }

                if(!math.all(intrestPoint.position == intrestPoints[i].position))
                {
                    continue;
                }

                var containsAlready = false;
                for(int j = 0; j < connectedPoints.Length; j++)
                {
                    if (connectedPoints[j].entity.Equals(intrestPointsEntities[i]))
                    {
                        containsAlready = true;
                        continue;
                    }
                }

                if (containsAlready)
                {
                    continue;
                }

                connectedPoints.Add(new ConnectedIntrestPoint
                {
                    entity = intrestPointsEntities[i],
                });
            }
        }
    }

    public struct RecalculateIntrestPointConnections : IComponentData
    {
        public bool recalculate;
    }
}