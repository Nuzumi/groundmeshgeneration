﻿using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using Unity.Burst;
using Unity.Entities;
using Unity.Transforms;

namespace Assets.Scripts.Systems
{

    public partial struct CartIconSpawnSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<Cart>();
            state.RequireForUpdate<CartIconCreateRequest>();
            state.RequireForUpdate<CartIconPrefab>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var prefabs = SystemAPI.GetSingletonBuffer<CartIconPrefab>(true);
            var query = SystemAPI.QueryBuilder().WithAll<Cart, CartIconCreateRequest>().Build();

            var carts = query.ToComponentDataArray<Cart>(state.WorldUpdateAllocator);
            var entities = query.ToEntityArray(state.WorldUpdateAllocator);

            for(int i = 0; i < carts.Length; i++)
            {
                var prefab = GetIconPrefab(carts[i].product, prefabs);
                var icon = state.EntityManager.Instantiate(prefab);
                state.EntityManager.AddComponentData(icon, new Parent
                {
                    Value = entities[i]
                });
                state.EntityManager.AddComponentData(entities[i], new CartIcon
                {
                    value = icon
                });

                SystemAPI.SetComponentEnabled<CartIconCreateRequest>(entities[i], false);
            }
        }

        private Entity GetIconPrefab(Product product, in DynamicBuffer<CartIconPrefab> prefabs)
        {
            for(int i = 0; i < prefabs.Length; i++)
            {
                if (prefabs[i].product == product)
                {
                    return prefabs[i].entityPrefab;
                }
            }

            return prefabs[0].entityPrefab;
        }
    }
}
