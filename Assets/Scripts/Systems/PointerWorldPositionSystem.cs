﻿using Assets.Scripts.Authorings;
using Assets.Scripts.Utils;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Assets.Scripts.Systems
{

    public partial struct PointerWorldPositionSystem : ISystem
    {

        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<PointerPosition>();
            state.RequireForUpdate<PointerWorldPosition>();
            state.RequireForUpdate<ClosestIntrestPosition>();
            state.EntityManager.CreateSingleton<WorldGenerationConfig>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var pointerPosition = SystemAPI.GetSingleton<PointerPosition>().value;
            var generationConfig = SystemAPI.GetSingleton<WorldGenerationConfig>();
            var closestIntrestPoint = SystemAPI.GetSingleton<ClosestIntrestPosition>();

            var groundHeight = GroundCalculationsUtils.GetGroundHeight(
                generationConfig.octaveFrequencies.AsReadOnly(),
                generationConfig.octaveAmplitudes.AsReadOnly(),
                pointerPosition,
                generationConfig.minValue,
                generationConfig.maxHeight,
                generationConfig.center, 
                generationConfig.reduceStartZoneDistance,
                generationConfig.reduceEndZoneDistance);

            var position = new float3(pointerPosition.x, groundHeight, pointerPosition.y);

            if(math.distancesq(position, closestIntrestPoint.position) < 0.1225f)
            {
                position = closestIntrestPoint.position;
            }

            SystemAPI.SetSingleton(new PointerWorldPosition
            {
                value = position
            });
        }
    }

    public struct WorldGenerationConfig : IComponentData
    {
        public NativeArray<float> octaveFrequencies;
        public NativeArray<float> octaveAmplitudes;
        public float minValue;
        public float moistureFrequency;
        public float maxMoisture;
        public float maxHeight;
        public float2 center;
        public float reduceStartZoneDistance;
        public float reduceEndZoneDistance;
    }
}