﻿using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;

namespace Assets.Scripts.Systems
{

    public partial struct CartCreationSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<Producer>();
            state.RequireForUpdate<Storage>();
            state.RequireForUpdate<Prefabs>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var cartPrefab = SystemAPI.GetSingleton<Prefabs>().cartPrefab;
            var query = SystemAPI.QueryBuilder().WithAll<Producer, Storage, IntrestPoint>().WithNone<UnitProducer>().Build();
            CreateCarts(ref state, cartPrefab, query);

            var unitCartPrefab = SystemAPI.GetSingleton<Prefabs>().unitCartPrefab;
            var unitProducerQuery = SystemAPI.QueryBuilder().WithAll<Producer, Storage, IntrestPoint, UnitProducer>().Build();
            CreateCarts(ref state, unitCartPrefab, unitProducerQuery);

            var cartsEntities = SystemAPI.QueryBuilder().WithAll<Cart, LinkedEntityGroup>().Build();
            state.EntityManager.RemoveComponent<LinkedEntityGroup>(cartsEntities);
        }

        private void CreateCarts(ref SystemState state, Entity cartPrefab, EntityQuery query)
        {
            var entities = query.ToEntityArray(state.WorldUpdateAllocator);

            for (int i = 0; i < entities.Length; i++)
            {
                var producers = SystemAPI.GetBuffer<Producer>(entities[i]);
                var producedProducts = new NativeHashSet<int>(producers.Length, Allocator.Temp);

                for (int j = 0; j < producers.Length; j++)
                {
                    producedProducts.Add((int)producers[j].product);
                }
                var storage = SystemAPI.GetBuffer<Storage>(entities[i]);
                var intrestPoint = SystemAPI.GetComponent<IntrestPoint>(entities[i]);

                for (int j = 0; j < storage.Length; j++)
                {
                    if (!producedProducts.Contains((int)storage[j].product))
                    {
                        continue;
                    }

                    if (storage[j].amount <= 0)
                    {
                        continue;
                    }

                    storage.ElementAt(j).amount--;
                    var cartEntity = state.EntityManager.Instantiate(cartPrefab);
                    SystemAPI.SetComponent(cartEntity, LocalTransform.FromPosition(intrestPoint.position));
                    SystemAPI.SetComponent(cartEntity, new Cart
                    {
                        amount = 1,
                        product = storage[j].product,
                        previousTarget = entities[i],
                        currentTarget = entities[i],
                        owner = intrestPoint.owner
                    });
                    SystemAPI.SetComponentEnabled<CartReachedTarget>(cartEntity, true);
                }

                producedProducts.Dispose();
            }
        }
    }
}