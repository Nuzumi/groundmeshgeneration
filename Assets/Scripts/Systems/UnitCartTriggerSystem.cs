﻿using Assets.Scripts.Authorings;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;

namespace Assets.Scripts.Systems
{

    [UpdateInGroup(typeof(PhysicsSystemGroup))]
    [UpdateAfter(typeof(PhysicsSimulationGroup))]
    public partial struct UnitCartTriggerSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<UnitCart>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            state.Dependency = new UnitCartTriggerJob
            {
                CartGroup = SystemAPI.GetComponentLookup<Cart>(true),
                UnitCartGroup = SystemAPI.GetComponentLookup<UnitCart>(false),
            }.Schedule(SystemAPI.GetSingleton<SimulationSingleton>(), state.Dependency);
        }
    }


    [BurstCompile]
    public struct UnitCartTriggerJob : ITriggerEventsJob
    {
        public ComponentLookup<UnitCart> UnitCartGroup;
        [ReadOnly] public ComponentLookup<Cart> CartGroup;

        public void Execute(TriggerEvent triggerEvent)
        {
            var entityA = triggerEvent.EntityA;
            var entityB = triggerEvent.EntityB;

            if(!UnitCartGroup.HasComponent(entityA) || !UnitCartGroup.HasComponent(entityB))
            {
                return;
            }

            var cartA = CartGroup[entityA];
            var cartB = CartGroup[entityB];

            if(cartA.owner == cartB.owner)
            {
                return;
            }

            if (!cartA.previousTarget.Equals(cartB.currentTarget))
            {
                return;
            }

            var unitCartA = UnitCartGroup[entityA];
            var unitCartB = UnitCartGroup[entityB];

            if(unitCartA.health == 0 || unitCartB.health == 0)
            {
                return;
            }

            unitCartA.health -= unitCartB.damage;
            unitCartB.health -= unitCartA.damage;

            UnitCartGroup[entityA] = unitCartA;
            UnitCartGroup[entityB] = unitCartB;
        }
    }
}