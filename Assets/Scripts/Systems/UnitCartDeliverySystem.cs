﻿using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using Unity.Burst;
using Unity.Entities;

namespace Assets.Scripts.Systems
{

    [UpdateBefore(typeof(CartMoveSystem))]
    public partial struct UnitCartDeliverySystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<Cart>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            bool changed = false;

            foreach (var (cart, entity) in SystemAPI
                .Query<Cart>()
                .WithAll<CartFinalTarget, CartReachedTarget, UnitCart>()
                .WithEntityAccess())
            {
                SystemAPI.GetComponentRW<IntrestPoint>(cart.finalTarget).ValueRW.owner = cart.owner;
                SystemAPI.SetComponentEnabled<CartReachedTarget>(entity, false);
                SystemAPI.SetComponentEnabled<CartFinalTarget>(entity, false);
                changed = true;
            }

            if (changed)
            {
                SystemAPI.GetSingletonRW<RecalculateOwnerBounds>().ValueRW.recalculate = true;
            }

        }
    }
}