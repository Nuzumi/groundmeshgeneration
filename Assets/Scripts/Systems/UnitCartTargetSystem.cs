﻿using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Assets.Scripts.Systems
{
    public partial struct UnitCartTargetSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var ecb = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(state.WorldUnmanaged);

            new UnitCartTargetJob
            {
                ConnectedIntrestPointGroup = SystemAPI.GetBufferLookup<ConnectedIntrestPoint>(true),
                ConsumerGroup = SystemAPI.GetBufferLookup<Consumer>(true),
                StorageGroup = SystemAPI.GetBufferLookup<Storage>(true),
                IntrestPointGroup = SystemAPI.GetComponentLookup<IntrestPoint>(true),
                commandBuffer = ecb.AsParallelWriter()
            }.ScheduleParallel();
        }
    }

    [WithAll(typeof(CartReachedTarget))]
    [WithAll(typeof(UnitCart))]
    [WithNone(typeof(CartFinalTarget))]
    [BurstCompile]
    public partial struct UnitCartTargetJob : IJobEntity
    {
        [ReadOnly] public BufferLookup<ConnectedIntrestPoint> ConnectedIntrestPointGroup;
        [ReadOnly] public BufferLookup<Consumer> ConsumerGroup;
        [ReadOnly] public BufferLookup<Storage> StorageGroup;
        [ReadOnly] public ComponentLookup<IntrestPoint> IntrestPointGroup;

        public EntityCommandBuffer.ParallelWriter commandBuffer;

        public void Execute(ref Cart cart, in Entity entity)
        {
            var queue = new NativeQueue<Node>(Allocator.TempJob);
            var routs = new NativeHashMap<Entity, Node>(50, Allocator.TempJob);

            var neighbours = ConnectedIntrestPointGroup[cart.currentTarget];
            for (int i = 0; i < neighbours.Length; i++)
            {
                var node = new Node
                {
                    value = neighbours[i].entity,
                    previous = cart.currentTarget,
                    cost = GetTravelsalCost(neighbours[i].entity, cart.currentTarget)
                };

                queue.Enqueue(node);
                routs.Add(node.value, node);
            }

            var processedEntities = new NativeHashSet<Entity>(50, Allocator.TempJob);
            var possibleDestinations = new NativeList<Entity>(Allocator.TempJob);

            while (queue.Count > 0)
            {
                var currentNode = queue.Dequeue();
                processedEntities.Add(currentNode.value);

                if (!HasSameOwner(currentNode.value, cart.owner))
                {
                    possibleDestinations.Add(currentNode.value);
                    continue;
                }

                var nodeNeighbours = ConnectedIntrestPointGroup[currentNode.value];
                for (int i = 0; i < nodeNeighbours.Length; i++)
                {
                    var neighbour = nodeNeighbours[i].entity;
                    if (processedEntities.Contains(neighbour))
                    {
                        continue;
                    }

                    var node = new Node
                    {
                        value = neighbour,
                        previous = currentNode.value,
                        cost = currentNode.cost + GetTravelsalCost(neighbour, currentNode.value)
                    };

                    queue.Enqueue(node);

                    if (!routs.ContainsKey(neighbour) || routs[neighbour].cost > node.cost)
                    {
                        routs[neighbour] = node;
                    }
                }
            }

            if (possibleDestinations.Length > 0)
            {
                Entity selectedTarget = Entity.Null;

                var possibleNodeDestinations = new NativeArray<Node>(possibleDestinations.Length, Allocator.TempJob);

                for (int i = 0; i < possibleDestinations.Length; i++)
                {
                    possibleNodeDestinations[i] = routs[possibleDestinations[i]];
                }

                possibleNodeDestinations.Sort();

                for (int i = 0; i < possibleNodeDestinations.Length; i++)
                {
                    if (possibleNodeDestinations[i].value.Equals(cart.finalTarget))
                    {
                        selectedTarget = cart.finalTarget;
                        break;
                    }
                }

                if (selectedTarget.Equals(Entity.Null))
                {
                    selectedTarget = possibleNodeDestinations[0].value;
                }

                var tmpTarget = selectedTarget;
                while (true)
                {
                    if (routs[tmpTarget].previous.Equals(cart.currentTarget))
                    {
                        break;
                    }

                    tmpTarget = routs[tmpTarget].previous;
                }

                cart.previousTarget = cart.currentTarget;
                cart.finalTarget = selectedTarget;
                cart.currentTarget = tmpTarget;
                if (selectedTarget.Equals(tmpTarget))
                {
                    commandBuffer.SetComponentEnabled<CartFinalTarget>(entity.Index, entity, true);
                }
                commandBuffer.SetComponentEnabled<CartReachedTarget>(entity.Index, entity, false);

                possibleNodeDestinations.Dispose();
            }

            queue.Dispose();
            routs.Dispose();
            processedEntities.Dispose();
            possibleDestinations.Dispose();
        }

        private float GetTravelsalCost(in Entity entity1, in Entity entity2)
        {
            var position1 = IntrestPointGroup[entity1].position;
            var position2 = IntrestPointGroup[entity2].position;

            if (math.all(position1 == position2))
            {
                return 0;
            }

            return math.distance(position1, position2);
        }

        private bool HasSameOwner(Entity entity, Owner owner)
        {
            var intrestPoint = IntrestPointGroup[entity];
            return intrestPoint.owner == owner;
        }
    }
}