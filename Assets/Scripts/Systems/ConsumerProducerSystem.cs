﻿using Assets.Scripts.BuildingControllers;
using Unity.Burst;
using Unity.Entities;

namespace Assets.Scripts.Systems
{
    public partial struct ConsumerProducerSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<Producer>();
            state.RequireForUpdate<Consumer>();
            state.RequireForUpdate<Storage>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            new ConsumerProducerJob
            {
                deltaTime = SystemAPI.Time.DeltaTime
            }.ScheduleParallel();
        }
    }

    [BurstCompile]
    public partial struct ConsumerProducerJob : IJobEntity
    {
        public float deltaTime;

        public void Execute(ref DynamicBuffer<Consumer> consumers, ref DynamicBuffer<Producer> producers, ref DynamicBuffer<Storage> storages)
        {
            if(!CanFunction(ref consumers, ref storages))
            { 
                return;
            }

            UpdateConsumers(ref consumers, ref storages);
            UpdateProducers(ref producers, ref storages);
        }

        [BurstCompile]
        private readonly bool CanFunction(ref DynamicBuffer<Consumer> consumers, ref DynamicBuffer<Storage> storages)
        {
            for(int i = 0; i < storages.Length; i++)
            {
                var minValue = IsConsumerProduct(ref consumers, storages[i].product) ? 1 : 0;

                if (storages[i].amount < minValue)
                {
                    return false;
                }
            }

            return true;
        }

        private readonly bool IsConsumerProduct(ref DynamicBuffer<Consumer> consumers, in Product product)
        {
            for(int i = 0; i < consumers.Length; i++)
            {
                if (consumers[i].product == product)
                {
                    return true;
                }
            }

            return false;
        }

        [BurstCompile]
        private readonly void UpdateConsumers(ref DynamicBuffer<Consumer> consumers, ref DynamicBuffer<Storage> storages)
        {
            for (var i = 0; i < consumers.Length; i++)
            {
                var consumer = consumers[i];
                consumer.consumptionProgress += consumer.consumptionRate * deltaTime;

                if (consumer.consumptionProgress > 1)
                {
                    consumer.consumptionProgress = 0;
                    ModifyStarageProduct(ref storages, consumer.product, -1);
                }

                consumers[i] = consumer;
            }
        }

        [BurstCompile]
        private readonly void UpdateProducers(ref DynamicBuffer<Producer> producers, ref DynamicBuffer<Storage> storages)
        {
            for (var i = 0; i < producers.Length; i++)
            {
                var producer = producers[i];
                producer.prodctionProgress += producer.productionRate * deltaTime;

                if (producer.prodctionProgress > 1)
                {
                    producer.prodctionProgress = 0;
                    ModifyStarageProduct(ref storages, producer.product, 1);
                }

                producers[i] = producer;
            }
        }

        [BurstCompile]
        private static void ModifyStarageProduct(ref DynamicBuffer<Storage> storages, Product product, int value)
        {
            for (int i = 0; i < storages.Length; i++)
            {
                if (storages[i].product == product)
                {
                    storages.ElementAt(i).amount += value;
                    return;
                }
            }
        }
    }
}