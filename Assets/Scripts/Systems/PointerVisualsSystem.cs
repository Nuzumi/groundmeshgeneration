﻿using Assets.Scripts.Authorings;
using Unity.Burst;
using Unity.Entities;
using Unity.Transforms;

namespace Assets.Scripts.Systems
{
    public partial struct PointerVisualsSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<PointerVisual>();
            state.RequireForUpdate<PointerWorldPosition>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var worldPosition = SystemAPI.GetSingleton<PointerWorldPosition>();
            var pointerEntity = SystemAPI.GetSingletonEntity<PointerVisual>();
            var pointerTransform = SystemAPI.GetComponentRW<LocalTransform>(pointerEntity);

            pointerTransform.ValueRW.Position = worldPosition.value;
        }
    }
}