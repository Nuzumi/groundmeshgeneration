﻿using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;

namespace Assets.Scripts.Systems
{
    public partial struct PointerClosestIntrestPointSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.EntityManager.CreateSingleton<ClosestIntrestPosition>();
            state.RequireForUpdate<PointerWorldPosition>();
            state.RequireForUpdate<IntrestPoint>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var closestIntrestPoint = SystemAPI.GetSingletonRW<ClosestIntrestPosition>();
            var pointerPosition = SystemAPI.GetSingleton<PointerWorldPosition>();

            float minDistanceSqr = float.MaxValue;
            float3 closestPosition = float3.zero;
            Entity closestEntity = Entity.Null;
            bool isBuilding = false;

            //todo if this it too slow on a main thread move it to job, divide it into 2 loops to query buildings and then roads
            foreach(var (intrestPoint, entity) in SystemAPI.Query<RefRO<IntrestPoint>>().WithEntityAccess())
            {
                var distanceSqr = math.distancesq(intrestPoint.ValueRO.position, pointerPosition.value);

                if (distanceSqr < minDistanceSqr)
                {
                    minDistanceSqr = distanceSqr;
                    closestPosition = intrestPoint.ValueRO.position;
                    closestEntity = entity;
                    isBuilding = SystemAPI.HasComponent<Building>(entity);
                }

                if (distanceSqr == minDistanceSqr && !isBuilding)
                {
                    minDistanceSqr = distanceSqr;
                    closestPosition = intrestPoint.ValueRO.position;
                    closestEntity = entity;
                    isBuilding = SystemAPI.HasComponent<Building>(entity);
                }
            }

            closestIntrestPoint.ValueRW.entity = closestEntity;
            closestIntrestPoint.ValueRW.position = closestPosition;
            closestIntrestPoint.ValueRW.distanceSqr = minDistanceSqr;
        }
    }

    public struct ClosestIntrestPosition : IComponentData
    {
        public Entity entity;
        public float3 position;
        public float distanceSqr;
    }
}