﻿using Assets.Scripts.Authorings;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;

namespace Assets.Scripts.Systems
{
    [UpdateBefore(typeof(CartIconDestroySystem))]
    public partial struct UnitCartDestroySystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<UnitCart>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var toDestroy = new NativeList<Entity>(state.WorldUpdateAllocator);

            foreach(var (unitCart, entity) in SystemAPI.Query<UnitCart>().WithEntityAccess())
            {
                if(unitCart.health <= 0)
                {
                    toDestroy.Add(entity);
                }
            }

            state.EntityManager.DestroyEntity(toDestroy.ToArray(state.WorldUpdateAllocator));
        }
    }
}