using Assets.Scripts.Authorings;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.Systems
{

    public partial class InputSystem : SystemBase
    {
        private Inputs.PointerActionsActions pointerActions;
        private Camera camera;

        protected override void OnCreate()
        {
            RequireForUpdate<PointerPosition>();
            pointerActions = new Inputs().PointerActions;
        }

        protected override void OnStartRunning()
        {
            camera = Camera.main;
            pointerActions.Enable();
        }

        protected override void OnUpdate()
        {
            var pointerPosition = SystemAPI.GetSingletonRW<PointerPosition>();
            var mousePosition = pointerActions.MousePosition.ReadValue<Vector2>();
            var worldPosition = camera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, camera.transform.position.y)); 
            pointerPosition.ValueRW.value = ((float3)worldPosition).xz;
        }
    }
}