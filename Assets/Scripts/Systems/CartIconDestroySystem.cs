﻿using Assets.Scripts.Authorings;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;

namespace Assets.Scripts.Systems
{
    [UpdateBefore(typeof(CartMoveSystem))]
    public partial struct CartIconDestroySystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<CartIcon>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var toDestroy = new NativeList<Entity>(state.WorldUpdateAllocator);

            foreach(var (icon, entity) in SystemAPI.Query<CartIcon>().WithEntityAccess().WithNone<Cart>())
            {
                toDestroy.Add(entity);
                toDestroy.Add(icon.value);
            }

            state.EntityManager.DestroyEntity(toDestroy.ToArray(state.WorldUpdateAllocator));
        }
    }
}
