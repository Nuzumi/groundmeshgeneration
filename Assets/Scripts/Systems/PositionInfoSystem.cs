﻿using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using Assets.Scripts.Utils;
using Unity.Burst;
using Unity.Entities;
using Unity.Mathematics;

namespace Assets.Scripts.Systems
{

    public partial struct PositionInfoSystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<ClosestIntrestPosition>();
            state.RequireForUpdate<PositionInfo>();
            state.RequireForUpdate<PointerWorldPosition>();
            state.RequireForUpdate<WorldGenerationConfig>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            var closestIntrestPoint = SystemAPI.GetSingleton<ClosestIntrestPosition>();
            var pointerWorldPosition = SystemAPI.GetSingleton<PointerWorldPosition>();
            var worldGenerationConfig = SystemAPI.GetSingleton<WorldGenerationConfig>();

            var buildingModeInfo = SystemAPI.GetSingleton<BuildingModePositionInfo>();
            var positionInfo = SystemAPI.GetSingletonRW<PositionInfo>();
            var intrestPointName = SystemAPI.GetSingletonRW<PositionIntrestPointName>();
            var positionInfoEntity = SystemAPI.GetSingletonEntity<PositionInfo>();
            var consumerDataBuffer = SystemAPI.GetBuffer<Consumer>(positionInfoEntity);
            var producerDataBuffer = SystemAPI.GetBuffer<Producer>(positionInfoEntity);
            var storageDataBuffer = SystemAPI.GetBuffer<Storage>(positionInfoEntity);

            positionInfo.ValueRW.height = pointerWorldPosition.value.y - worldGenerationConfig.minValue;
            positionInfo.ValueRW.heightPercent = positionInfo.ValueRO.height / (worldGenerationConfig.maxHeight - worldGenerationConfig.minValue);
            positionInfo.ValueRW.moisture = 1 - GroundCalculationsUtils.GetMoiustureValue(pointerWorldPosition.value.xz, worldGenerationConfig.moistureFrequency);
            positionInfo.ValueRW.moisturePercent = positionInfo.ValueRO.moisture / worldGenerationConfig.maxMoisture;
            positionInfo.ValueRW.hasIntrestPoint = math.all(closestIntrestPoint.position ==  pointerWorldPosition.value);
            positionInfo.ValueRW.hasBuilding = positionInfo.ValueRO.hasIntrestPoint && SystemAPI.HasComponent<Building>(closestIntrestPoint.entity);

            if (buildingModeInfo.isInBuildingMode)
            {
                return;
            }

            consumerDataBuffer.Clear();
            producerDataBuffer.Clear();
            storageDataBuffer.Clear();

            if (!positionInfo.ValueRO.hasIntrestPoint 
                || closestIntrestPoint.entity.Equals(Entity.Null) 
                || !SystemAPI.HasComponent<Building>(closestIntrestPoint.entity))
            {
                return;
            }

            var intrestPoint = SystemAPI.GetComponent<IntrestPoint>(closestIntrestPoint.entity);
            intrestPointName.ValueRW.name = intrestPoint.name;

            var intrestPointConsumer = SystemAPI.GetBuffer<Consumer>(closestIntrestPoint.entity);
            for(int i = 0; i < intrestPointConsumer.Length; i++)
            {
                consumerDataBuffer.Add(intrestPointConsumer[i]);
            }

            var intrestPointProducer = SystemAPI.GetBuffer<Producer>(closestIntrestPoint.entity);
            for(int i = 0; i < intrestPointProducer.Length; i++)
            {
                producerDataBuffer.Add(intrestPointProducer[i]);
            }

            var intrestPointStorage = SystemAPI.GetBuffer<Storage>(closestIntrestPoint.entity);
            for(int i = 0; i < intrestPointStorage.Length; i++)
            {
                storageDataBuffer.Add(intrestPointStorage[i]);
            }
        }
    }
}