﻿using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using Unity.Burst;
using Unity.Entities;

namespace Assets.Scripts.Systems
{

    [UpdateBefore(typeof(CartIconDestroySystem))]
    public partial struct CartDeliverySystem : ISystem
    {
        [BurstCompile]
        public void OnCreate(ref SystemState state)
        {
            state.RequireForUpdate<Cart>();
            state.RequireForUpdate<Storage>();
            state.RequireForUpdate<CurrencyConfig>();
        }

        [BurstCompile]
        public void OnUpdate(ref SystemState state)
        {
            foreach(var cart in SystemAPI.Query<Cart>().WithAll<CartFinalTarget, CartReachedTarget>().WithNone<UnitCart>())
            {
                var storage = SystemAPI.GetBuffer<Storage>(cart.finalTarget);

                for(int i = 0; i < storage.Length; i++)
                {
                    if (storage[i].product == cart.product)
                    {
                        storage.ElementAt(i).amount++;
                        break;
                    }
                }
            }

            var cartDeliveredQuery = SystemAPI.QueryBuilder()
                .WithAll<Cart, CartFinalTarget, CartReachedTarget>()
                .WithNone<UnitCart>()
                .Build();
            SystemAPI.GetSingletonRW<CurrencyConfig>().ValueRW.value += cartDeliveredQuery.CalculateEntityCount();
            state.EntityManager.DestroyEntity(cartDeliveredQuery);
        }
    }
}