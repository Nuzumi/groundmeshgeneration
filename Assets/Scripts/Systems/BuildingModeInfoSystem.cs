﻿using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using Assets.Scripts.Utils;
using Unity.Entities;

namespace Assets.Scripts.Systems
{
    public partial class BuildingModeInfoSystem : SystemBase
    {
        protected override void OnCreate()
        {
            RequireForUpdate<BuildingModePositionInfo>();
            RequireForUpdate<PositionInfo>();
            RequireForUpdate<BuildingModeData>();
        }

        protected override void OnUpdate()
        {
            var buildingModeInfo = SystemAPI.GetSingleton<BuildingModePositionInfo>();

            if (!buildingModeInfo.isInBuildingMode)
            {
                return;
            }

            var infoEntity = SystemAPI.GetSingletonEntity<BuildingModePositionInfo>();
            var buildingData = EntityManager.GetComponentData<BuildingModeData>(infoEntity).buildingData;
            var buildingConfig = buildingData.BuildingConfig;
            var positionInfo = SystemAPI.GetSingleton<PositionInfo>();
            SystemAPI.GetSingletonRW<PositionIntrestPointName>().ValueRW.name = buildingData.BuildingName;

            var consumerDataBuffer = SystemAPI.GetBuffer<Consumer>(infoEntity);
            var producerDataBuffer = SystemAPI.GetBuffer<Producer>(infoEntity);
            var storageDataBuffer = SystemAPI.GetBuffer<Storage>(infoEntity);

            consumerDataBuffer.Clear();
            producerDataBuffer.Clear();
            storageDataBuffer.Clear();

            foreach(var consumerConfig in buildingConfig.Consumer)
            {
                consumerDataBuffer.Add(new Consumer
                {
                    consumptionRate = BuildingUtils.GetRate(consumerConfig, positionInfo.moisturePercent, positionInfo.heightPercent),
                    product = consumerConfig.product,
                });
            }

            foreach(var producerConfig in buildingConfig.Producer)
            {
                producerDataBuffer.Add(new Producer
                {
                    product = producerConfig.product,
                    productionRate = BuildingUtils.GetRate(producerConfig, positionInfo.moisturePercent, positionInfo.heightPercent),
                });
            }

            foreach(var storageConfig in buildingConfig.DefaultStorage)
            {
                storageDataBuffer.Add(new Storage
                {
                    amount = storageConfig.defaultAmount,
                    product = storageConfig.product,
                });
            }
        }
    }
}