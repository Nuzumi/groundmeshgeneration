using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    [BurstCompile]
    public static class GroundCalculationsUtils
    {

        [BurstCompile]
        public static float GetGroundHeight(
            in NativeArray<float>.ReadOnly octaveFrequencyModifiers,
            in NativeArray<float>.ReadOnly octaveAmplitudeModifiers,
            in float2 position,
            in float minValue,
            in float maxValue,
            in float2 center, 
            in float reduceStartZoneDistance,
            in float reduceEndZoneDistance)
        {
            var noiseAmplitudeModifier = 1f;
            var value = 0f;
            var noisePosition = position;
            for (int i = 0; i < octaveFrequencyModifiers.Length; i++)
            {
                noisePosition *= octaveFrequencyModifiers[i];
                noiseAmplitudeModifier *= octaveAmplitudeModifiers[i];
                value += ((noise.cnoise(noisePosition) + 0.6f) / 1.2f) * noiseAmplitudeModifier;
            }

            var distanceToCenter = math.distance(position, center);

            var reduceZoneDistance = distanceToCenter - reduceStartZoneDistance;
            var endDistance = reduceEndZoneDistance - reduceStartZoneDistance;
            var reducePercent = reduceZoneDistance / endDistance;

            if (reducePercent < 0)
            {
                return math.max(value, minValue);
            }
            else if(reducePercent >= 1)
            {
                return minValue;
            }
            else
            {
                var reduceValue = math.lerp(0, maxValue - minValue, reducePercent);
                value -= reduceValue;
            }

            return math.max(value, minValue);
        }

        [BurstCompile]
        public static float GetMoiustureValue(in float2 position, float frequency)
        {
            return (noise.cnoise(position * frequency) + 0.6f) / 1.2f;
        }
    }
}