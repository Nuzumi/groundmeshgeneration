﻿using Unity.Mathematics;

namespace Assets.Scripts.Utils
{
    public static class MathUtils
    {
        public static int2 ToInt(this float2 vector) => new int2((int)vector.x, (int)vector.y);
    }
}
