﻿using Assets.Scripts.BuildingControllers;
using UnityEngine;
using static Assets.Scripts.BuildingsConfig.BuildingConfig;

namespace Assets.Scripts.Utils
{
    public static class BuildingUtils
    {
        public static float GetRate(ProductData productData, float moisture, float height)
        {
            var baseRate = productData.baseRate;
            var moistureModifier = productData.moistureModifierCurve.Evaluate(moisture);
            var heightModifier = productData.heightModifierCurve.Evaluate(height);
            return baseRate * heightModifier * moistureModifier;
        }

        public static Color ToColor(this Owner owner) => owner switch
        {
            Owner.Owner1 => new Color(1, 0, 0, 1),
            Owner.Owner2 => new Color(0, 1, 0, 1),
            Owner.Owner3 => new Color(0, 0, 1, 1),
            Owner.Owner4 => new Color(1, 1, 0, 1),
            Owner.Owner5 => new Color(1, 0, 1, 1),
            Owner.Owner6 => new Color(0, 1, 1, 1),
            _ => throw new System.NotImplementedException(),
        };
    }
}
