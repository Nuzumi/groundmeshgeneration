﻿using Assets.Scripts.BuildingControllers;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.Authorings
{
    public class ConfigAuthoring : MonoBehaviour
    {
        public int BaseCurrencyValue;

        public class Baker : Baker<ConfigAuthoring>
        {
            public override void Bake(ConfigAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.None);
                AddComponent(entity, new CurrencyConfig
                {
                    value = authoring.BaseCurrencyValue,
                });
                AddComponent<PositionInfo>(entity);
                AddComponent<BuildingModePositionInfo>(entity);
                AddComponent<PositionIntrestPointName>(entity);
                AddComponent<BuildingCostInfo>(entity);
                AddComponent<RecalculateOwnerBounds>(entity);
                AddBuffer<Storage>(entity);
                AddBuffer<Consumer>(entity);
                AddBuffer<Producer>(entity);
                AddBuffer<RecalculateOwneerBoundsPosition>(entity);

                AddComponent<BuildingModeData>(entity);
            }
        }
    }

    public struct CurrencyConfig : IComponentData
    {
        public int value;
    }

    public struct PositionInfo : IComponentData
    {
        public float height;
        public float moisture;
        public float heightPercent;
        public float moisturePercent;
        public bool hasIntrestPoint;
        public bool hasBuilding;
    }

    public struct BuildingModePositionInfo : IComponentData
    {
        public bool isInBuildingMode;
        public bool isInRoadBuildingMode;
    }

    public struct BuildingCostInfo : IComponentData
    {
        public int value;
    }

    public struct PositionIntrestPointName : IComponentData
    {
        public FixedString32Bytes name;
    }

    public struct RecalculateOwnerBounds : IComponentData
    {
        public bool recalculate;
    }

    public struct RecalculateOwneerBoundsPosition : IBufferElementData
    {
        public float3 position;
    }

    public class BuildingModeData : IComponentData
    {
        public BuildingData buildingData;
    }
}
