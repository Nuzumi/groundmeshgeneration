﻿using Assets.Scripts.BuildingControllers;
using System;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.Authorings
{
    public class CartIconConfigAuthoring : MonoBehaviour
    {
        public List<CartIconPrefabData> cartIcons;

        public class Baker : Baker<CartIconConfigAuthoring>
        {
            public override void Bake(CartIconConfigAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.None);
                var buffer = AddBuffer<CartIconPrefab>(entity);

                foreach(var cartIcon in authoring.cartIcons)
                {
                    buffer.Add(new CartIconPrefab
                    {
                        product = cartIcon.product,
                        entityPrefab = GetEntity(cartIcon.prefab, TransformUsageFlags.Dynamic)
                    });
                }
            }
        }

        [Serializable]
        public class CartIconPrefabData
        {
            public Product product;
            public GameObject prefab;
        }
    }
    
    public struct CartIconPrefab : IBufferElementData
    {
        public Product product;
        public Entity entityPrefab;
    }
}
