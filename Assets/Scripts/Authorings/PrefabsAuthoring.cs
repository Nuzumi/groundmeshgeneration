﻿using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.Authorings
{

    public class PrefabsAuthoring : MonoBehaviour
    {
        public GameObject cartPrefab;
        public GameObject unitCartPrefab;

        public class Baker : Baker<PrefabsAuthoring>
        {
            public override void Bake(PrefabsAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.None);
                AddComponent(entity, new Prefabs
                {
                    cartPrefab = GetEntity(authoring.cartPrefab, TransformUsageFlags.Dynamic),
                    unitCartPrefab = GetEntity(authoring.unitCartPrefab, TransformUsageFlags.Dynamic),
                });
            }
        }
    }

    public struct Prefabs : IComponentData
    {
        public Entity cartPrefab;
        public Entity unitCartPrefab;
    }
}
