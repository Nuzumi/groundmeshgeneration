﻿using Assets.Scripts.BuildingControllers;
using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.Authorings
{

    public class CartAuthoring : MonoBehaviour
    {
        public float cartSpeed;

        public class Baker : Baker<CartAuthoring>
        {
            public override void Bake(CartAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent(entity, new CartConfig
                {
                    speed = authoring.cartSpeed
                });
                AddComponent<Cart>(entity);
                AddComponent<CartReachedTarget>(entity);
                AddComponent<CartFinalTarget>(entity);
                AddComponent<CartIconCreateRequest>(entity);
                SetComponentEnabled<CartReachedTarget>(entity, false);
                SetComponentEnabled<CartFinalTarget>(entity, false);
                SetComponentEnabled<CartIconCreateRequest>(entity, true);
            }
        }
    }

    public struct Cart : IComponentData
    {
        public Product product;
        public Owner owner;
        public int amount;
        public Entity previousTarget;
        public Entity currentTarget;
        public Entity finalTarget;
    }

    public struct CartConfig : IComponentData 
    {
        public float speed;
    }

    public struct CartFinalTarget : IComponentData, IEnableableComponent
    {
    }

    public struct CartReachedTarget : IComponentData, IEnableableComponent
    {
    }

    public struct CartIconCreateRequest : IComponentData, IEnableableComponent
    {

    }

    public struct CartIcon : ICleanupComponentData
    {
        public Entity value;
    }
}
