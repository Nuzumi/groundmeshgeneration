﻿using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.Authorings
{
    public class PointerVisualsAuthoring : MonoBehaviour
    {
        public class Baker : Baker<PointerVisualsAuthoring>
        {
            public override void Bake(PointerVisualsAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent<PointerVisual>(entity);
            }
        }
    }

    public struct PointerVisual : IComponentData
    {

    }
}
