﻿using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.Authorings
{
    public class UnitCartAuthoring : MonoBehaviour
    {
        public float health;
        public float damage;

        public class Baker : Baker<UnitCartAuthoring>
        {
            public override void Bake(UnitCartAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.Dynamic);
                AddComponent(entity, new UnitCart
                {
                    health = authoring.health,
                    damage = authoring.damage,
                });
            }
        }
    }

    public struct UnitCart : IComponentData
    {
        public float health;
        public float damage;
    }
}
