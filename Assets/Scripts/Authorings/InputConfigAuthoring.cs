﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.Authorings
{


    public class InputConfigAuthoring : MonoBehaviour
    {
        public class Baker : Baker<InputConfigAuthoring>
        {
            public override void Bake(InputConfigAuthoring authoring)
            {
                var entity = GetEntity(TransformUsageFlags.None);
                AddComponent<PointerPosition>(entity);
                AddComponent<PointerWorldPosition>(entity);
            }
        }
    }

    public struct PointerPosition : IComponentData
    {
        public float2 value;
    }

    public struct PointerWorldPosition : IComponentData
    {
        public float3 value;
    }
}
