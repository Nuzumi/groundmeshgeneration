﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Ui.BuildingsMenu
{
    public class BuildingOption : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI costText;
        [SerializeField] private Button button;

        private Action selectAction;

        public void Initialise(Action selectAction, string name, string cost)
        {
            this.selectAction = selectAction;
            nameText.text = name;
            costText.text = cost;
        }

        private void Awake()
        {
            button.onClick.AddListener(OptionSelected);
        }

        private void OnDestroy()
        {
            button.onClick.RemoveAllListeners();
        }

        private void OptionSelected() => selectAction();
    }
}