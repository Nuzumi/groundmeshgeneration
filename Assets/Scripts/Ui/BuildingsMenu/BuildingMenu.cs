using Assets.Scripts.BuildingControllers;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Ui.BuildingsMenu
{

    public class BuildingMenu : MonoBehaviour
    {
        [SerializeField] private BuildingOption buildingOptionPrefab;
        [SerializeField] private Transform optionParent;

        private BuildingControllers.BuildingControllers buildingControllers;
        private BuildingSelectionController buildingSelectionController;

        [Inject]
        public void Inject(BuildingControllers.BuildingControllers buildingControllers, BuildingSelectionController buildingSelectionController)
        {
            this.buildingControllers = buildingControllers;
            this.buildingSelectionController = buildingSelectionController;
            CreateButtons();
        }

        private void CreateButtons()
        {
            int idx = 0;
            for(int i = 0; i < buildingControllers.Controllers.Count; i++)
            {
                var controller = buildingControllers.Controllers[i];

                for(int j = 0; j < controller.BuildingsCount; j++ )
                {
                    var name = buildingControllers.Controllers[i].GetBuildingName(j);
                    var option = Instantiate(buildingOptionPrefab, optionParent);
                    var optionIdx = idx;
                    option.Initialise(() => OptionSelected(optionIdx), name, (optionIdx + 1).ToString());
                    idx++;
                }
            }
        }

        private void OptionSelected(int idx) => buildingSelectionController.OnOptionSelected(idx);
    }
}