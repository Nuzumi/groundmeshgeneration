using Assets.Scripts.Authorings;
using TMPro;
using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.Ui.CoinsDisplays
{
    public class CoinDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI coinsAmount;

        private EntityManager entityManager;
        private EntityQuery coinsQuery;

        private void Awake()
        {
            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            coinsQuery = entityManager.CreateEntityQuery(typeof(CurrencyConfig));
        }

        private void Update()
        {
            if (coinsQuery.IsEmpty)
            {
                return;
            }

            var currency = coinsQuery.GetSingleton<CurrencyConfig>();
            coinsAmount.text = currency.value.ToString();
        }
    }
}