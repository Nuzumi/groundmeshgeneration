﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.Ui.InfoPanels
{

    public class InfoPanelConsumerData : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI productName;
        [SerializeField] private TextMeshProUGUI productionPercent;
        [SerializeField] private TextMeshProUGUI consumptionRate;
        [SerializeField] private TextMeshProUGUI storedAmount;

        public void SetData(string productName, float percent, float rate, int stored)
        {
            this.productName.text = productName;
            productionPercent.text = $"{(int)(percent * 100)}%";
            consumptionRate.text = $"{rate:F2} / S";
            storedAmount.text = stored.ToString();
        }
    }
}