using Assets.Scripts.Authorings;
using Assets.Scripts.BuildingControllers;
using Assets.Scripts.Data;
using System.Collections.Generic;
using TMPro;
using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.Ui.InfoPanels
{
    public class InfoPanel : MonoBehaviour
    {
        private const string InfoNpanelName = "Info";
        private const int PreCreatedDataPrefabs = 3;

        [SerializeField] private TextMeshProUGUI buildingName;
        [SerializeField] private TextMeshProUGUI height;
        [SerializeField] private TextMeshProUGUI moisture;
        [SerializeField] private TextMeshProUGUI buildingCost;
        [SerializeField] private GameObject buildingCostGameObject;
        [SerializeField] private Transform producerPanel;
        [SerializeField] private Transform consumerPanel;
        [SerializeField] private Transform neededPanel;
        [SerializeField] private InfoPanelProducerData producerDataPrefab;
        [SerializeField] private InfoPanelConsumerData consumerDataPrefab;
        [SerializeField] private InfoPanelNeededAmountData neededAmountDataPrefab;
        [SerializeField] private MoistureLevelData moistureLevelData;

        private List<InfoPanelProducerData> producerList = new ();
        private List<InfoPanelConsumerData> consumerList = new ();
        private List<InfoPanelNeededAmountData> neededList = new ();

        private EntityManager entityManager;
        private EntityQuery infoQuery;

        private void Awake()
        {
            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            infoQuery = entityManager.CreateEntityQuery(typeof(PositionInfo), typeof(BuildingModePositionInfo));

            CreateDataPrefabs(producerList, producerDataPrefab, producerPanel);
            CreateDataPrefabs(consumerList, consumerDataPrefab, consumerPanel);
            CreateDataPrefabs(neededList, neededAmountDataPrefab, neededPanel);
        }

        private void Update()
        {
            if (infoQuery.IsEmpty)
            {
                return;
            }

            buildingCostGameObject.SetActive(false);
            var info = infoQuery.GetSingleton<PositionInfo>();
            SetPointInfo(info);
            TurnOffDynamicData();
            var infoEntity = infoQuery.GetSingletonEntity();

            SetName(infoEntity);

            if (info.hasBuilding)
            {
                if (HasNegativeStorage(entityManager.GetBuffer<Storage>(infoEntity)))
                {
                    SetNeededProductsInfo(infoEntity);
                }
                else
                {
                    SetProducersInfo(infoEntity);
                    SetConsumersInfo(infoEntity);
                }

                return;
            }

            var buildModeInfo = infoQuery.GetSingleton<BuildingModePositionInfo>();
            
            if (buildModeInfo.isInRoadBuildingMode)
            {
                SetCost(infoEntity);
                return;
            }

            if (buildModeInfo.isInBuildingMode)
            {
                SetCost(infoEntity);
                SetNeededProductsInfo(infoEntity);
                SetProducersInfo(infoEntity);
                SetConsumersInfo(infoEntity);
                return;
            }
        }

        private void SetName(Entity infoEntity)
        {
            var name = entityManager.GetComponentData<PositionIntrestPointName>(infoEntity).name.ToString();

            if (string.IsNullOrEmpty(name))
            {
                buildingName.text = InfoNpanelName;
            }
            else
            {
                buildingName.text = name.ToString();
            }
        }

        private void SetCost(Entity infoEntity)
        {
            buildingCostGameObject.SetActive(true);
            var cost = entityManager.GetComponentData<BuildingCostInfo>(infoEntity).value;
            buildingCost.text = cost.ToString();
        }

        private void SetPointInfo(in PositionInfo info)
        {
            var heightInMeters = (int)(info.height * 100);
            height.text = $"{heightInMeters}m asl";
            moisture.text = $"{(int)(info.moisture * 100)}%";
        }

        private void SetConsumersInfo(Entity infoEntity)
        {
            var viewIdx = 0;
            var consumerBuffer = entityManager.GetBuffer<Consumer>(infoEntity);
            var storageBuffer = entityManager.GetBuffer<Storage>(infoEntity);
            for (int i = 0; i < consumerBuffer.Length; i++)
            {
                var consumer = consumerBuffer[i];
                var storage = GetStorageForProduct(storageBuffer, consumer.product);
                consumerList[viewIdx].SetData(consumer.product.ToString(), consumer.consumptionProgress, consumer.consumptionRate, storage.amount);
                consumerList[viewIdx].gameObject.SetActive(true);
                viewIdx++;
            }
        }

        private void SetProducersInfo(Entity infoEntity)
        {
            var viewIdx = 0;
            var producerBuffer = entityManager.GetBuffer<Producer>(infoEntity);
            for (int i = 0; i < producerBuffer.Length; i++)
            {
                var producer = producerBuffer[i];
                producerList[viewIdx].SetData(producer.product.ToString(), producer.prodctionProgress, producer.productionRate);
                producerList[viewIdx].gameObject.SetActive(true);
                viewIdx++;
            }
        }

        private void SetNeededProductsInfo(Entity infoEntity)
        {
            var viewIdx = 0;
            var storageBuffer = entityManager.GetBuffer<Storage>(infoEntity);
            for(int i = 0; i < storageBuffer.Length; i++)
            {
                if (storageBuffer[i].amount < 0)
                {
                    var storage = storageBuffer[i];
                    neededList[viewIdx].SetData(storage.product.ToString(), storage.amount);
                    neededList[viewIdx].gameObject.SetActive(true);
                    viewIdx++;
                }
            }
        }

        private void CreateDataPrefabs<T>(List<T> list, T prefab, Transform parent) where T : MonoBehaviour
        {
            for(int i = 0; i < PreCreatedDataPrefabs; i++)
            {
                var createdPrefab = Instantiate(prefab, parent);
                createdPrefab.gameObject.SetActive(false);
                list.Add(createdPrefab);
            }
        }

        private void TurnOffDynamicData()
        {
            producerList.ForEach(data => data.gameObject.SetActive(false));
            consumerList.ForEach(data => data.gameObject.SetActive(false));
            neededList.ForEach(data => data.gameObject.SetActive(false));
        }

        private Storage GetStorageForProduct(DynamicBuffer<Storage> storages, Product product)
        {
            for(int i = 0; i < storages.Length; i++) 
            {
                if (storages[i].product == product)
                {
                    return storages[i];
                }
            }

            return new Storage();
        }

        private bool HasNegativeStorage(DynamicBuffer<Storage> storages)
        {
            for(int i = 0; i < storages.Length; i++)
            {
                if (storages[i].amount < 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
}