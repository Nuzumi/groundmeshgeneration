﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.Ui.InfoPanels
{
    public class InfoPanelNeededAmountData : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI productName;
        [SerializeField] private TextMeshProUGUI neededAmount;

        public void SetData(string productName, int amount)
        {
            this.productName.text = productName;
            neededAmount.text = (-amount).ToString();
        }
    }
}