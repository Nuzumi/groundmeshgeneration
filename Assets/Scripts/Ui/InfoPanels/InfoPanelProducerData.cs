﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.Ui.InfoPanels
{
    public class InfoPanelProducerData : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI productName;
        [SerializeField] private TextMeshProUGUI productionPercent;
        [SerializeField] private TextMeshProUGUI productionRate;

        public void SetData(string productName, float percent, float rate)
        {
            this.productName.text = productName;
            productionPercent.text = $"{(int)(percent * 100)}%";
            productionRate.text = $"{rate:F2} / S";
        }
    }
}