﻿using Assets.Scripts.BuildingControllers;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BuildingsConfig
{
    [Serializable]
    public class BuildingConfig   
    {
        [field: SerializeField] public List<ProductData> Producer { get; protected set; }
        [field: SerializeField] public List<ProductData> Consumer { get; protected set; }
        [field: SerializeField] public List<StorageData> DefaultStorage { get; protected set; }


        [Serializable]
        public class ProductData
        {
            public Product product;
            public float baseRate;
            public AnimationCurve moistureModifierCurve;
            public AnimationCurve heightModifierCurve;
        }

        [Serializable]
        public class StorageData
        {
            public Product product;
            public int defaultAmount;
        }
    }
}
