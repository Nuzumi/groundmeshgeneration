﻿using Assets.Scripts.Utils;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

namespace Assets.Scripts.MeshControllers
{
    [BurstCompile]
    public partial struct GroundUvJob : IJobFor
    {
        public float2 offset;
        public float moistureFrequency;
        public float minValue;
        public float maxValue;
        [ReadOnly] public NativeArray<float3>.ReadOnly vertices;
        public NativeArray<float2> uvCoordinates;

        public void Execute(int index)
        {
            var maxOfsset = maxValue - minValue;
            var heightOfsset = vertices[index].y - minValue;
            var heightPercent = heightOfsset / maxOfsset;

            var noisePosition = vertices[index].xz + offset;
            var moistureValue = GroundCalculationsUtils.GetMoiustureValue(noisePosition, moistureFrequency);

            uvCoordinates[index] = new float2(moistureValue, heightPercent);
        }
    }

}
