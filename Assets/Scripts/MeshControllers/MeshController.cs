﻿using Assets.Scripts.Systems;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Scripts.MeshControllers
{

    public class MeshController : MonoBehaviour
    {
        public MeshFilter meshFilter;
        public int surfaceActualSize = 10;
        public int surfacePoints = 100;

        public float moistureNoiseFrequency;
        public List<float> octaveFrequencyModifiers;
        public List<float> octaveAmplitudeModifiers;
        [Range(0,1)]
        public float minValuePercent;
        public Vector2 center;
        public float reduceZoneStartDistanc;
        public float reduceZoneEndDistanc;
        public bool recalculate;
        public int ownerBoundDataIdx;
        public int currentOwnerBoundDataVersion;

        private NativeArray<float3> vertices;
        private NativeArray<float2> uvCooridnates;
        private NativeArray<float> octaveFrequencyModifiersArray;
        private NativeArray<float> octaveAmplitudeModifiersArray;
        private Mesh mesh;
        private float minValue;
        private float maxValue;
        private EntityManager entityManager;
        private EntityQuery worldGenerationConfigQuery;
        private EntityQuery ownerBoundDataQuery;

        private void Awake()
        {
            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            worldGenerationConfigQuery = entityManager.CreateEntityQuery(typeof(WorldGenerationConfig));
            ownerBoundDataQuery = entityManager.CreateEntityQuery(typeof(OwnerBoundData));
        }

        public void UpdateBounds(in OwnerBoundData ownerBoundData)
        {
            var meshDat = ownerBoundData.boundMeshDatas[ownerBoundDataIdx];

            if (meshDat.version > currentOwnerBoundDataVersion)
            {
                UpdateMeshColros(meshDat);
                currentOwnerBoundDataVersion = meshDat.version;
            }
        }

        public void InitMesh(int actualSize, int surfacePoints, int OwnerDataIdx)
        {
            surfaceActualSize = actualSize;
            this.surfacePoints = surfacePoints;

            mesh = CreateMesh();
            meshFilter.mesh = mesh;
            vertices = new NativeArray<float3>(mesh.vertices.Length, Allocator.Persistent);
            uvCooridnates = new NativeArray<float2>(mesh.vertices.Length, Allocator.Persistent);
            octaveFrequencyModifiersArray = new NativeArray<float>(octaveAmplitudeModifiers.Count, Allocator.Persistent);
            octaveAmplitudeModifiersArray = new NativeArray<float>(octaveAmplitudeModifiers.Count, Allocator.Persistent);
            NativeArray<float3>.Copy(mesh.vertices.Select(v => (float3)v).ToArray(), vertices);
            InitOwnerBoundsData(OwnerDataIdx);
        }

        public void GenerateMeshHeight()
        {
            maxValue = GetMaxHeightValue();
            minValue = maxValue * minValuePercent;
            UpdateMesh();
        }

        private void InitOwnerBoundsData(int ownerDataIdx)
        {
            if (ownerBoundDataQuery.IsEmpty)
            {
                Debug.LogException(new Exception("No OwnerBoundData to initialise"));
                return;
            }

            ownerBoundDataIdx = ownerDataIdx;
            var flatPosition = transform.position;
            flatPosition.y = 0;
            var data = ownerBoundDataQuery.GetSingletonRW<OwnerBoundData>();
            data.ValueRW.boundMeshDatas[ownerDataIdx] = new OwnerBoundMeshData
            {
                aabb = new AABB()
                {
                    Center = flatPosition,
                    Extents = new float3(surfaceActualSize / 2, 0, surfaceActualSize / 2)
                },
                gridSize = surfacePoints,
                id = ownerDataIdx,
                colors = new NativeArray<Color>(surfacePoints * surfacePoints, Allocator.Persistent)
            };
        }

        private void UpdateMeshColros(OwnerBoundMeshData data) => mesh.SetColors(data.colors);

        private Mesh CreateMesh()
        {
            Mesh newMesh = new()
            {
                indexFormat = IndexFormat.UInt32
            };

            vertices = new NativeArray<float3>(surfacePoints * surfacePoints, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);
            var index = 0;
            for (var i = 0; i < surfacePoints; i++)
            {
                for (var j = 0; j < surfacePoints; j++)
                {
                    float x = MapValue(i, 0.0f, surfacePoints - 1, -surfaceActualSize / 2.0f, surfaceActualSize / 2.0f);
                    float z = MapValue(j, 0.0f, surfacePoints - 1, -surfaceActualSize / 2.0f, surfaceActualSize / 2.0f);
                    vertices[index++] = new Vector3(x, 0f, z);
                }
            }

            var indices = new int[(surfacePoints - 1) * (surfacePoints - 1) * 6];
            index = 0;
            for (var i = 0; i < surfacePoints - 1; i++)
            {
                for (var j = 0; j < surfacePoints - 1; j++)
                {
                    var baseIndex = i * surfacePoints + j;
                    indices[index++] = baseIndex;
                    indices[index++] = baseIndex + 1;
                    indices[index++] = baseIndex + surfacePoints + 1;
                    indices[index++] = baseIndex;
                    indices[index++] = baseIndex + surfacePoints + 1;
                    indices[index++] = baseIndex + surfacePoints;
                }
            }

            newMesh.SetVertices(vertices);
            newMesh.triangles = indices;
            newMesh.RecalculateNormals();
            meshFilter.mesh = newMesh;

            return newMesh;
        }

        static float MapValue(float refValue, float refMin, float refMax, float targetMin, float targetMax)
        {
            return targetMin + (refValue - refMin) * (targetMax - targetMin) / (refMax - refMin);
        }

        private void UpdateMesh()
        {
            for (int i = 0; i < octaveAmplitudeModifiers.Count; i++)
            {
                octaveAmplitudeModifiersArray[i] = octaveAmplitudeModifiers[i];
                octaveFrequencyModifiersArray[i] = octaveFrequencyModifiers[i];
            }

            if (!worldGenerationConfigQuery.IsEmpty)
            {
                var config = worldGenerationConfigQuery.GetSingletonRW<WorldGenerationConfig>();
                config.ValueRW.minValue = minValue;
                config.ValueRW.moistureFrequency = moistureNoiseFrequency;
                config.ValueRW.octaveFrequencies = octaveFrequencyModifiersArray;
                config.ValueRW.octaveAmplitudes = octaveAmplitudeModifiersArray;
                config.ValueRW.maxMoisture = 1;
                config.ValueRW.maxHeight = GetMaxHeightValue();
                config.ValueRW.center = center;
                config.ValueRW.reduceEndZoneDistance = reduceZoneEndDistanc;
                config.ValueRW.reduceStartZoneDistance = reduceZoneStartDistanc;
            }

            CalculateVerticesHeight();
            mesh.SetVertices(vertices);

            var uvJob = CalculateMeshUvs();
            mesh.RecalculateNormals();
            uvJob.Complete();
            mesh.SetUVs(0, uvCooridnates);
        }

        private void CalculateVerticesHeight()
        {
            var offset = ((float3)transform.position).xz;
            new GroundHeightJob
            {
                vertices = vertices,
                minValue = minValue,
                offset = offset,
                octaveAmplitudeModifiers = octaveAmplitudeModifiersArray.AsReadOnly(),
                octaveFrequencyModifiers = octaveFrequencyModifiersArray.AsReadOnly(),
                center = center,
                maxValue = maxValue,
                reduceEndZoneDistance = reduceZoneEndDistanc,
                reduceStartZoneDistance = reduceZoneStartDistanc
            }.ScheduleParallel(vertices.Length, 1000, new JobHandle()).Complete();
        }

        private JobHandle CalculateMeshUvs()
        {
            var offset = ((float3)transform.position).xz;
            return new GroundUvJob
            {
                offset = offset,
                vertices = vertices.AsReadOnly(),
                minValue = minValue,
                maxValue = GetMaxHeightValue(),
                uvCoordinates = uvCooridnates,
                moistureFrequency = moistureNoiseFrequency
            }.ScheduleParallel(uvCooridnates.Length, 1000, new JobHandle());
        }

        private float GetMaxHeightValue()
        {
            var value = 0f;
            var modifier = 1f;

            for(int i = 0; i < octaveAmplitudeModifiers.Count; i++)
            {
                modifier *= octaveAmplitudeModifiers[i];
                value += modifier;
            }

            return value;
        }

        
    }

}
