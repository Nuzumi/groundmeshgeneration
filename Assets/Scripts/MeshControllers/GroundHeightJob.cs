﻿using Assets.Scripts.Utils;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

namespace Assets.Scripts.MeshControllers
{
    [BurstCompile]
    public partial struct GroundHeightJob : IJobFor
    {
        public float2 offset;
        public float3 resultOffset;
        [ReadOnly] public NativeArray<float>.ReadOnly octaveFrequencyModifiers;
        [ReadOnly] public NativeArray<float>.ReadOnly octaveAmplitudeModifiers;
        public float minValue;
        public float maxValue;
        public float2 center;
        public float reduceStartZoneDistance;
        public float reduceEndZoneDistance;

        public NativeArray<float3> vertices;

        public void Execute(int index)
        {
            var position = vertices[index];
            var noisePosition = position.xz + offset;
            var height = GroundCalculationsUtils.GetGroundHeight(
                octaveFrequencyModifiers,
                octaveAmplitudeModifiers,
                noisePosition,
                minValue,
                maxValue, 
                center,
                reduceStartZoneDistance,
                reduceEndZoneDistance);
            position.y = height;
            vertices[index] = position + resultOffset;
        }
    }
}
