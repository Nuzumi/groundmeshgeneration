﻿using Assets.Scripts.Systems;
using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Assets.Scripts.MeshControllers
{
    public class MeshPositionsController : MonoBehaviour
    {
        [SerializeField] private MeshController planePrefab;
        [SerializeField] private Transform cameraTransform;
        [SerializeField] private int planeSize;
        [SerializeField] private int planeSurfacePointsCount;
        [SerializeField] private int planesGridSize;

        private List<(Vector3Int position, MeshController plane)> planes;
        private EntityQuery boundsDataQuery;

        private void Start()
        {
            InitialiseOwnerBoundData();
            planes = new List<(Vector3Int position, MeshController plane)> ();

            int i = 0;
            var halfSize = planesGridSize / 2;
            for(int x = -halfSize; x < halfSize; x++)
            {
                for (int z = -halfSize; z < halfSize; z++)
                {
                    var position = new Vector3Int(x * planeSize, 0, z * planeSize);
                    var plane = Instantiate(planePrefab, position, planePrefab.transform.rotation);
                    plane.InitMesh(planeSize, planeSurfacePointsCount, i);
                    plane.GenerateMeshHeight();
                    planes.Add((position, plane));
                    i++;
                }
            }
        }

        private void InitialiseOwnerBoundData()
        {
            var entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            boundsDataQuery = entityManager.CreateEntityQuery(typeof(OwnerBoundData));

            if (boundsDataQuery.IsEmpty)
            {
                Debug.LogException(new Exception("No OwnerBoundData to initialise"));
                return;
            }

            var data = boundsDataQuery.GetSingletonRW<OwnerBoundData>();
            data.ValueRW.boundMeshDatas = new NativeArray<OwnerBoundMeshData>(planesGridSize * planesGridSize, Allocator.Persistent);
        }

        private void Update()
        {
            if (boundsDataQuery.IsEmpty)
            {
                return;
            }

            var boundsData = boundsDataQuery.GetSingleton<OwnerBoundData>();

            for (int i = 0; i < planes.Count; i++)
            {
                planes[i].plane.UpdateBounds(boundsData);
            }
        }
    }
}
