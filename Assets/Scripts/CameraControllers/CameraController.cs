using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.CameraControllers
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private float minMoveSpeed;
        [SerializeField] private float maxMoveSpeed;
        [SerializeField] private float zoomSpeed;
        [SerializeField] private float minCameraSize;
        [SerializeField] private float maxCameraSize;
        [SerializeField] new private Camera camera;

        private Inputs.CameraControllsActions cameraControlls;
        private float zoomedMoveSpeed;

        private void Awake()
        {
            cameraControlls = new Inputs().CameraControlls;
        }

        private void Start()
        {
            cameraControlls.Enable();
        }

        private void Update()
        {
            CalculateZoomedMoveSpeed();
            MoveCamera();
            ZoomCamera();
        }

        private void CalculateZoomedMoveSpeed()
        {
            var zoomLevel = camera.orthographicSize;
            var zoomPercent = math.unlerp(minCameraSize, maxCameraSize, zoomLevel);
            zoomedMoveSpeed = math.lerp(minMoveSpeed, maxMoveSpeed, zoomPercent);
        }

        private void MoveCamera()
        {
            var move = cameraControlls.Move.ReadValue<Vector2>();
            transform.position += new Vector3(move.x, 0, move.y) * (zoomedMoveSpeed * Time.deltaTime);
        }

        private void ZoomCamera()
        {
            var zoom = cameraControlls.Zoom.ReadValue<Vector2>().y;

            if (zoom == 0)
            {
                return;
            }

            if (zoom > 0 && camera.orthographicSize < maxCameraSize)
            {
                ApplyZoom();
            }

            if (zoom < 0 && camera.orthographicSize > minCameraSize)
            {
                ApplyZoom();
            }

            void ApplyZoom()
            {
                camera.orthographicSize += zoom * zoomSpeed * Time.deltaTime;

                var size = camera.orthographicSize;
                var clampedSize = math.min(math.max(size, minCameraSize), maxCameraSize);
                camera.orthographicSize = clampedSize;
            }
        }
    }
}