﻿using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.Roads
{
    public class CurveSegment : RoadSegment
    {
        private readonly Vector3 handle;
        private readonly int curveSegments;

        public CurveSegment(Vector3 start, Vector3 end, float roadWodth, Vector3 handle, int curveSegments) : base(start, end, roadWodth)
        {
            this.handle = handle;
            this.curveSegments = curveSegments;
        }

        protected override Vector3 GetCurrentPosition(float t)
        {
            var l1 = math.lerp(start, handle, t);
            var l2 = math.lerp(handle, end, t);
            return math.lerp(l1, l2, t);
        }

        protected override int GetSegmentsCount() => curveSegments;
    }
}
