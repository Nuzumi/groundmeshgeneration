﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.Roads
{
    public class StartSegment : LineSegment
    {
        public StartSegment(Vector3 start, Vector3 end, float roadWodth, int lineSubSegementsPerUnitLength) : base(start, end, roadWodth, lineSubSegementsPerUnitLength)
        {
        }

        public override void AddSegmentData(NativeList<float3> vertices, List<int> triangles)
        {
            var startToEndDirection = math.normalize(end - start);

            var v1 = start + (Vector3)math.cross(startToEndDirection, Vector3.up) * roadWidth;
            var v2 = start - (Vector3)math.cross(startToEndDirection, Vector3.up) * roadWidth;

            vertices.Add(v1);
            vertices.Add(v2);

            base.AddSegmentData(vertices, triangles);
        }
    }
}
