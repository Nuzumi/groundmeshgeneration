﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.Roads
{
    public abstract class RoadSegment
    {
        protected readonly Vector3 start;
        protected readonly Vector3 end;
        protected readonly float roadWidth;

        public RoadSegment(Vector3 start, Vector3 end, float roadWidth)
        {
            this.start = start;
            this.end = end;
            this.roadWidth = roadWidth;
        }

        public virtual void AddSegmentData(NativeList<float3> vertices, List<int> triangles)
        {
            var segments = GetSegmentsCount();
            var t = 0f;
            var deltaT = 1f / segments;

            Vector3 lastPosition = start;

            for (int i = 0; i < segments; i++)
            {
                t += deltaT;
                var currrentPosition = GetCurrentPosition(t);
                AddLineData(vertices, triangles, lastPosition, currrentPosition);
                lastPosition = currrentPosition;
            }
        }

        protected abstract Vector3 GetCurrentPosition(float t);

        protected abstract int GetSegmentsCount();

        private void AddLineData(NativeList<float3> vertices, List<int> triangles, Vector3 start, Vector3 end)
        {
            var v1Index = vertices.Length - 2;
            var v2Index = vertices.Length - 1;
            var v3Index = vertices.Length;
            var v4Index = vertices.Length + 1;

            var startToEndDirection = math.normalize(end - start);
            vertices.Add(end + (Vector3)math.cross(startToEndDirection, Vector3.up) * roadWidth);
            vertices.Add(end - (Vector3)math.cross(startToEndDirection, Vector3.up) * roadWidth);

            triangles.Add(v2Index);
            triangles.Add(v1Index);
            triangles.Add(v3Index);

            triangles.Add(v2Index);
            triangles.Add(v3Index);
            triangles.Add(v4Index);
        }
    }
}
