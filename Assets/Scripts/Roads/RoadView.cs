﻿using Assets.Scripts.BuildingControllers;
using Assets.Scripts.MeshControllers;
using Assets.Scripts.Systems;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Scripts.Roads
{

    public class RoadView : MonoBehaviour
    {
        public IReadOnlyList<Vector3> Points => points;

        [SerializeField] private MeshFilter meshFilter;
        [SerializeField] private float maxHalfeCurveLength;
        [SerializeField] private int curveSubSegements;
        [SerializeField] private int lineSubSegementsPerUnitLength;
        [SerializeField] private float roadWidth;
        [SerializeField] private float yOffset;

        private Mesh mesh;
        private List<Vector3> points = new ();
        private List<RoadSegment> segments = new ();

        private EntityManager entityManager;
        private EntityQuery generationConfigQuery;
        private EntityArchetype roadArchetype;

        private void Awake()
        {
            mesh = new()
            {
                indexFormat = IndexFormat.UInt32
            };
            mesh.MarkDynamic();
            meshFilter.mesh = mesh;

            entityManager = World
                .DefaultGameObjectInjectionWorld
                .EntityManager;
            generationConfigQuery = 
                entityManager
                .CreateEntityQuery(typeof(WorldGenerationConfig));

            roadArchetype = entityManager.CreateArchetype(typeof(IntrestPoint), typeof(Road), typeof(ConnectedIntrestPoint));
        }

        public void CreateEntities(Owner owner)
        {
            var entities = new List<Entity>();

            foreach (var point in points)
            {
                var entity = entityManager.CreateEntity(roadArchetype);
                entityManager.SetComponentData(entity, new IntrestPoint
                {
                    position = point,
                    owner = owner
                });
                entities.Add(entity);
            }

            entityManager.GetBuffer<ConnectedIntrestPoint>(entities[0]).Add(new ConnectedIntrestPoint
            {
                entity = entities[1]
            });

            for (int i = 1; i < entities.Count - 1; i++)
            {
                var entity = entities[i];
                var buffer = entityManager.GetBuffer<ConnectedIntrestPoint>(entity);
                buffer.Add(new ConnectedIntrestPoint { entity = entities[i + 1] });
                buffer.Add(new ConnectedIntrestPoint { entity = entities[i - 1] });
            }

            entityManager.GetBuffer<ConnectedIntrestPoint>(entities[^1]).Add(new ConnectedIntrestPoint
            {
                entity = entities[^2]
            });
        }

        public void AddPoint(Vector3 point)
        {
            points.Add(point);

            if (points.Count > 1)
            {
                GenerateMesh();
            }
        }

        public void DisplayNextPoint(Vector3 point)
        {
            var lastPoint = points[points.Count - 1];
            var distance = math.distance(lastPoint, point);

            if(distance < .3f)
            {
                return;
            }

            points.Add(point);
            GenerateMesh();
            points.RemoveAt(points.Count - 1);
        }

        public void GenerateMesh()
        {
            if(points.Count <= 1)
            {
                return;
            }

            segments.Clear();
            PointsSegmentation();

            var vertices = new NativeList<float3>(Allocator.TempJob);
            var triangles = new List<int>();

            foreach(var segment in segments)
            {
                segment.AddSegmentData(vertices, triangles);
            }

            var verticesArray = vertices.AsArray();
            ApplyGroundHeight(verticesArray);

            mesh.triangles = new int[0];
            mesh.SetVertices(verticesArray);
            mesh.triangles = triangles.ToArray();
            mesh.RecalculateNormals();
            vertices.Dispose();
        }

        private void PointsSegmentation()
        {
            float3 lastPoint = points[0];

            for(int i = 1; i < points.Count;  i++)
            {
                if(i == points.Count - 1)
                {
                    if(i == 1)
                    {
                        segments.Add(new StartSegment(lastPoint, points[i], roadWidth, lineSubSegementsPerUnitLength));
                    }
                    else
                    {
                        segments.Add(new LineSegment(lastPoint, points[i], roadWidth, lineSubSegementsPerUnitLength));
                    }
                }
                else
                {
                    var currentPoint = (float3)points[i];
                    var nextPoint = (float3)points[i + 1];
                
                    var lastToCurrentDistance = math.distance(lastPoint, currentPoint);
                    var currentToNextDisntace = math.distance(currentPoint, nextPoint);

                    if(lastToCurrentDistance > maxHalfeCurveLength)
                    {
                        var lastToCurrentDirection = math.normalize(currentPoint - lastPoint);
                        var segmentEndPoint = currentPoint - (lastToCurrentDirection * maxHalfeCurveLength) ;

                        if(i == 1)
                        {
                            segments.Add(new StartSegment(lastPoint, segmentEndPoint, roadWidth, lineSubSegementsPerUnitLength));
                        }
                        else
                        {
                            segments.Add(new LineSegment(lastPoint, segmentEndPoint, roadWidth, lineSubSegementsPerUnitLength));
                        }

                        lastPoint = segmentEndPoint;
                    }

                    if(currentToNextDisntace > maxHalfeCurveLength)
                    {
                        var curretnToNextDirection = math.normalize(nextPoint - currentPoint);
                        var curveEndPoint = currentPoint + (curretnToNextDirection * maxHalfeCurveLength);
                        segments.Add(new CurveSegment(lastPoint, curveEndPoint, roadWidth, currentPoint, curveSubSegements));
                        lastPoint = curveEndPoint;
                    }
                    else
                    {
                        var curretnToNextDirection = math.normalize(nextPoint - currentPoint);
                        var curveEndPoint = currentPoint + curretnToNextDirection * currentToNextDisntace / 2;
                        segments.Add(new CurveSegment(lastPoint, curveEndPoint, roadWidth, currentPoint, curveSubSegements));
                        lastPoint = curveEndPoint;
                    }
                }
            }
        }

        private void ApplyGroundHeight(NativeArray<float3> array)
        {
            if (generationConfigQuery.IsEmpty)
            {
                return;
            }

            var config = generationConfigQuery.GetSingleton<WorldGenerationConfig>();

            new GroundHeightJob
            {
                minValue = config.minValue,
                octaveAmplitudeModifiers = config.octaveAmplitudes.AsReadOnly(),
                octaveFrequencyModifiers = config.octaveFrequencies.AsReadOnly(),
                resultOffset = new float3(0, yOffset, 0),
                vertices = array,
                maxValue = config.maxHeight,
                center = config.center,
                reduceEndZoneDistance = config.reduceEndZoneDistance,
                reduceStartZoneDistance = config.reduceStartZoneDistance
            }.Schedule(array.Length, new JobHandle())
            .Complete();
        }
    }
}
