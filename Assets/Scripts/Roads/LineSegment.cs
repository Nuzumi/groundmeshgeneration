﻿using Unity.Mathematics;
using UnityEngine;

namespace Assets.Scripts.Roads
{
    public class LineSegment : RoadSegment
    {
        protected readonly int lineSubSegementsPerUnitLength;

        public LineSegment(Vector3 start, Vector3 end, float roadWodth, int lineSubSegementsPerUnitLength) : base(start, end, roadWodth)
        {
            this.lineSubSegementsPerUnitLength = lineSubSegementsPerUnitLength;
        }

        protected override Vector3 GetCurrentPosition(float t) => math.lerp(start, end, t);

        protected override int GetSegmentsCount()
        {
            var distance = math.distance(start, end);
            return math.max((int)(distance * lineSubSegementsPerUnitLength), 1);
        }
    }
}
