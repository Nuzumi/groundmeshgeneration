﻿using UnityEngine;

namespace Assets.Scripts.Data
{
    [CreateAssetMenu(menuName = "Data/" + nameof(MoistureLevelData), fileName = nameof(MoistureLevelData))]
    public class MoistureLevelData : ScriptableObject
    {
        [SerializeField] private float maxLowMoistureLevel;
        [SerializeField] private float maxMidMoistureLevel;

        public int GetMoistureLevel(float moisturePercent) => moisturePercent switch
        {
            _ when moisturePercent< maxLowMoistureLevel => 0,
            _ when moisturePercent< maxMidMoistureLevel => 1,
            _ => 2
        };
}
}
