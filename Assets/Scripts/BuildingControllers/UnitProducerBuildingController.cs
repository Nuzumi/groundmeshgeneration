﻿using Unity.Entities;

namespace Assets.Scripts.BuildingControllers
{
    public class UnitProducerBuildingController : VillagebuildingController
    {
        protected override EntityArchetype GetArchetype()
        {
            return entityManager.CreateArchetype(typeof(IntrestPoint),
                typeof(ConnectedIntrestPoint),
                typeof(Storage),
                typeof(Producer),
                typeof(Consumer),
                typeof(Building),
                typeof(UnitProducer));
        }
    }
}