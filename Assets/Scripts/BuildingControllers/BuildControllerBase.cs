﻿using Assets.Scripts.Authorings;
using Assets.Scripts.MeshControllers;
using Assets.Scripts.Systems;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.BuildingControllers
{

    public abstract class BuildControllerBase : MonoBehaviour
    {
        [SerializeField] protected Owner owner = Owner.Owner1;

        public abstract int BuildingsCount { get; }

        protected EntityManager entityManager;
        protected EntityQuery pointerPositionQuery;
        protected EntityQuery infoQuery;
        protected int currentBuilding;

        private EntityQuery recalculateConnectionsConfigQuery;
        private EntityQuery generationConfigQuery;
        private EntityQuery currencyQuery;

        public abstract void Activate();
        public abstract void DeActivate();

        public abstract string GetBuildingName(int idx);

        public void SetActiveBuilding(int idx)
        {
            currentBuilding = idx;
            OnActiveBuildingSelected();
        }

        protected abstract void OnActiveBuildingSelected();

        protected virtual void Awake()
        {
            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            recalculateConnectionsConfigQuery = entityManager.CreateEntityQuery(typeof(RecalculateIntrestPointConnections));
            pointerPositionQuery = entityManager.CreateEntityQuery(typeof(PointerWorldPosition));
            generationConfigQuery = entityManager.CreateEntityQuery(typeof(WorldGenerationConfig));
            currencyQuery = entityManager.CreateEntityQuery(typeof(CurrencyConfig));
            infoQuery = entityManager.CreateEntityQuery(typeof(BuildingModePositionInfo), typeof(PositionInfo), typeof(BuildingCostInfo), typeof(RecalculateOwnerBounds), typeof(RecalculateOwneerBoundsPosition));

        }

        protected void TriggerIntrestPointRecalculations()
        {
            if (recalculateConnectionsConfigQuery.IsEmpty)
            {
                return;
            }

            var config = recalculateConnectionsConfigQuery.GetSingletonRW<RecalculateIntrestPointConnections>();
            config.ValueRW.recalculate = true;
        }

        protected bool IsMouseOverUi()
        {
            return EventSystem.current.IsPointerOverGameObject();
        }

        protected void RemoveCurrency(int currencyAmount)
        {
            if (currencyQuery.IsEmpty)
            {
                return;
            }

            currencyQuery.GetSingletonRW<CurrencyConfig>().ValueRW.value -= currencyAmount;
        }

        protected bool HaveEnoughCurrency(int neededCurrency)
        {
            if (currencyQuery.IsEmpty)
            {
                return false;
            }

            var currency = currencyQuery.GetSingleton<CurrencyConfig>().value;

            return currency >= neededCurrency;
        }

        protected bool CanBuildAtPosition(float3 position, bool isWaterBuilding)
        {
            if (generationConfigQuery.IsEmpty)
            {
                return false;
            }

            var config = generationConfigQuery.GetSingleton<WorldGenerationConfig>();

            if (isWaterBuilding)
            {
                return position.y == config.minValue;
            }
            else
            {
                return position.y > config.minValue;
            }
        }

        protected bool CanBuildAtPositions(List<float3> positions, bool isWaterBuilding)
        {
            if (generationConfigQuery.IsEmpty)
            {
                return false;
            }

            var config = generationConfigQuery.GetSingleton<WorldGenerationConfig>();
            var pointsArray = new NativeArray<float3>(positions.Count, Allocator.TempJob);
            NativeArray<float3>.Copy(positions.ToArray(), pointsArray);

            new GroundHeightJob
            {
                minValue = config.minValue,
                octaveAmplitudeModifiers = config.octaveAmplitudes.AsReadOnly(),
                octaveFrequencyModifiers = config.octaveFrequencies.AsReadOnly(),
                vertices = pointsArray,
                maxValue = config.maxHeight,
                center = config.center,
                reduceEndZoneDistance = config.reduceEndZoneDistance,
                reduceStartZoneDistance = config.reduceStartZoneDistance,
            }
            .Schedule(pointsArray.Length, new JobHandle())
            .Complete();

            for(int i = 0; i < pointsArray.Length; i++)
            {
                var pointHeight = pointsArray[i].y;
                if (isWaterBuilding)
                {
                    if(pointHeight != config.minValue)
                    {
                        return false;
                    }
                }
                else
                {
                    if(pointHeight <= config.minValue)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}