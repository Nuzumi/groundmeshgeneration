﻿using Assets.Scripts.Roads;
using UnityEngine;

namespace Assets.Scripts.BuildingControllers
{
    [CreateAssetMenu(menuName = "Data/" + nameof(RoadData), fileName = nameof(RoadData))]
    public class RoadData : ScriptableObject
    {
        [field: SerializeField] public string RoadName { get; private set; }
        [field: SerializeField] public RoadView RoadViewPrefab { get; private set; }
        [field: SerializeField] public float XZDistanceCost { get; private set; }
        [field: SerializeField] public float YDistanceCost { get; private set; }
        [field: SerializeField] public bool IsWaterRoad { get; private set; }
    }
}