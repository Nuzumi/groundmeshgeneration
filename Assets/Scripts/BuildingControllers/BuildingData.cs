﻿using Assets.Scripts.BuildingsConfig;
using UnityEngine;

namespace Assets.Scripts.BuildingControllers
{

    [CreateAssetMenu(menuName = "Data/"+ nameof(BuildingData), fileName = nameof(BuildingData))]
    public class BuildingData : ScriptableObject
    {
        [field: SerializeField] public string BuildingName { get; private set; }
        [field: SerializeField] public bool IsWaterbuilding { get; private set; }
        [field: SerializeField] public GameObject Prefab { get; private set; }
        [field: SerializeField] public BuildingConfig BuildingConfig { get; private set; }
        [field: SerializeField] public int CurrencyCost { get; private set; }
    }
}