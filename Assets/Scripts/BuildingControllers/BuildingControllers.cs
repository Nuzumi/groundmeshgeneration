﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BuildingControllers
{
    public class BuildingControllers : MonoBehaviour
    {
        [field: SerializeField] public List<BuildControllerBase> Controllers { get; private set; }
    }
}