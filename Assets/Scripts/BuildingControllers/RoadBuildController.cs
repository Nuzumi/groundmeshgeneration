using Assets.Scripts.Authorings;
using Assets.Scripts.Roads;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.BuildingControllers
{

    public class RoadBuildController : BuildControllerBase
    {
        private const float MinHeightCheckDistnace = 0.02f;

        public override int BuildingsCount => roads.Count;

        [SerializeField] private List<RoadData> roads;

        private Inputs.PointerActionsActions pointerActions;
        private RoadView roadView;

        public override void Activate()
        {
            pointerActions.Enable();
            SetBuildingModeData(true);
        }

        public override void DeActivate()
        {
            pointerActions.Disable();
            SetBuildingModeData(false);

            if (roadView != null )
            {
                if (roadView.Points.Count <= 1)
                {
                    Destroy(roadView.gameObject);
                }

                if (roadView.Points.Count > 1)
                {
                    roadView.CreateEntities(owner);
                    roadView.GenerateMesh();
                    TriggerIntrestPointRecalculations();
                }
            }

            roadView = null;
        }

        protected override void OnActiveBuildingSelected()
        {
            DeActivate();
            Activate();
        }

        protected override void Awake()
        {
            base.Awake();
            pointerActions = new Inputs().PointerActions;

            pointerActions.PointerAction.performed += PointerAction;
        }

        private void Update()
        {

            if (roadView == null)
            {
                return;
            }

            if (pointerPositionQuery.IsEmpty)
            {
                return;
            }

            var pointerPosition = pointerPositionQuery.GetSingleton<PointerWorldPosition>().value;
            roadView.DisplayNextPoint(pointerPosition);

            var cost = GetRoadCost(pointerPosition);
            var costInfo = infoQuery.GetSingletonRW<BuildingCostInfo>();
            costInfo.ValueRW.value = cost;
        }

        private void PointerAction(InputAction.CallbackContext context)
        {
            if (IsMouseOverUi())
            {
                return;
            }

            if (pointerPositionQuery.IsEmpty)
            {
                return;
            }

            var pointerPosition = pointerPositionQuery.GetSingleton<PointerWorldPosition>().value;

            if (!CanBuildAtPosition(pointerPosition, false))
            {
                return;
            }

            if (roadView == null)
            {
                roadView = Instantiate(roads[currentBuilding].RoadViewPrefab);
            }


            if(roadView.Points.Count >= 1)
            {
                float3 lastPoint = roadView.Points[^1];
                var currentPoint = pointerPosition;
                var direction = math.normalize(currentPoint - (float3)lastPoint);
                var pointsCount = math.floor(math.distance(currentPoint, lastPoint) / MinHeightCheckDistnace);

                var list = new List<float3>();

                for(int i = 1; i < pointsCount; i++)
                {
                    list.Add(lastPoint + direction * (MinHeightCheckDistnace * i));
                }

                if (!CanBuildAtPositions(list, false))
                {
                    return;
                }

                var cost = GetRoadCost(pointerPosition);

                if (!HaveEnoughCurrency(cost))
                {
                    return;
                }

                RemoveCurrency(cost);
            }

            roadView.AddPoint(pointerPosition);
        }

        public override string GetBuildingName(int idx) => roads[idx].RoadName;

        private int GetRoadCost(float3 currentPosition)
        {
            var data = roads[currentBuilding];
            float3 lastPoint = roadView.Points.Last();
            var xzDistance = math.distance(currentPosition.xz, lastPoint.xz);
            var yDistance = math.distance(currentPosition.y, lastPoint.y);
            var xzCost = xzDistance * data.XZDistanceCost;
            var yCost = yDistance * data.YDistanceCost;

            return (int)math.floor(xzCost + yCost);
        }

        private void SetBuildingModeData(bool isActive)
        {
            if (infoQuery.IsEmpty)
            {
                return;
            }

            var info = infoQuery.GetSingletonRW<BuildingModePositionInfo>();
            info.ValueRW.isInRoadBuildingMode = isActive;
        }
    }
}