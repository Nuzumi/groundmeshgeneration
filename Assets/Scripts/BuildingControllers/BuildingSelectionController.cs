using System;
using UnityEngine.InputSystem;
using Zenject;

namespace Assets.Scripts.BuildingControllers
{

    public class BuildingSelectionController : IInitializable, IDisposable
    {
        private readonly BuildingControllers buildingControllers;
        private Inputs.BuildingSelectionActions buildingSelection;

        public BuildingSelectionController(BuildingControllers buildingControllers)
        {
            buildingSelection = new Inputs().BuildingSelection;
            buildingSelection.Option1.performed += (_) => OnOptionSelected(0);
            buildingSelection.Option2.performed += (_) => OnOptionSelected(1);
            buildingSelection.Option3.performed += (_) => OnOptionSelected(2);
            buildingSelection.Option4.performed += (_) => OnOptionSelected(3);
            buildingSelection.Option5.performed += (_) => OnOptionSelected(4);
            buildingSelection.CancleAction.performed += OnCancelPressed;
            this.buildingControllers = buildingControllers;
        }

        public void Initialize()
        {
            buildingSelection.Enable();
        }

        public void Dispose()
        {
            buildingSelection.Disable();
        }

        public void OnOptionSelected(int optionIdx)
        {
            var currentControllerIndex = optionIdx;

            buildingControllers.Controllers.ForEach(controller => controller.DeActivate());

            for (int i = 0; i < buildingControllers.Controllers.Count; i++)
            {
                var controller = buildingControllers.Controllers[i];
                if(currentControllerIndex >= controller.BuildingsCount)
                {
                    currentControllerIndex -= controller.BuildingsCount;
                }
                else
                {
                    controller.SetActiveBuilding(currentControllerIndex);
                    controller.Activate();
                    return;
                }
            }
        }

        private void OnCancelPressed(InputAction.CallbackContext context)
        {
            buildingControllers.Controllers.ForEach(controller => controller.DeActivate());
        }
    }
}