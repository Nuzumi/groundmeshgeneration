﻿using Assets.Scripts.Authorings;
using Assets.Scripts.Utils;
using System.Collections.Generic;
using System.Linq;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.BuildingControllers
{

    public class VillagebuildingController : BuildControllerBase
    {
        public override int BuildingsCount => buildings.Count;

        [SerializeField] private List<BuildingData> buildings;

        private Inputs.PointerActionsActions pointerActions;
        private EntityArchetype villageArchetype;
        private GameObject buildingView;

        public override void Activate()
        {
            pointerActions.Enable();
            SetBuildingModeData(true);
            buildingView = Instantiate(buildings[currentBuilding].Prefab);
        }

        public override void DeActivate()
        {
            pointerActions.Disable();
            SetBuildingModeData(false);
            Destroy(buildingView);
        }

        protected override void Awake()
        {
            base.Awake();
            pointerActions = new Inputs().PointerActions;
            villageArchetype = GetArchetype(); 
            pointerActions.PointerAction.performed += PointerAction;
        }

        protected virtual EntityArchetype GetArchetype()
        {
            return entityManager.CreateArchetype(typeof(IntrestPoint),
                typeof(ConnectedIntrestPoint),
                typeof(Storage),
                typeof(Producer),
                typeof(Consumer),
                typeof(Building));
        }

        private void Update()
        {
            if(buildingView == null)
            {
                return;
            }

            if (pointerPositionQuery.IsEmpty)
            {
                return;
            }

            var pointerPosition = pointerPositionQuery.GetSingleton<PointerWorldPosition>().value;
            buildingView.SetActive(CanBuildAtPosition(pointerPosition, buildings[currentBuilding].IsWaterbuilding));
            buildingView.transform.position = pointerPosition;
        }

        private void PointerAction(InputAction.CallbackContext context)
        {
            if (IsMouseOverUi())
            {
                return;
            }

            if (pointerPositionQuery.IsEmpty)
            {
                return;
            }

            var buildingData = buildings[currentBuilding];

            if (!HaveEnoughCurrency(buildingData.CurrencyCost))
            {
                return;
            }

            if (DoesBuildingExistsOnPosition())
            {
                return;
            }

            var pointerPosition = pointerPositionQuery.GetSingleton<PointerWorldPosition>().value;

            if(!CanBuildAtPosition(pointerPosition, buildingData.IsWaterbuilding))
            {
                return;
            }

            Instantiate(buildingData.Prefab, pointerPosition, buildingData.Prefab.transform.rotation);
            var entity = entityManager.CreateEntity(villageArchetype);
            entityManager.SetComponentData(entity, new IntrestPoint
            {
                position = pointerPosition,
                name = buildingData.name,
                owner = owner
            });

            var positionInfo = infoQuery.GetSingleton<PositionInfo>();

            AddProducer(entity, positionInfo);
            AddConsumer(entity, positionInfo);
            AddStorage(entity);

            TriggerIntrestPointRecalculations();
            TriggerOwnerBoudsRebuild(pointerPosition);
            RemoveCurrency(buildingData.CurrencyCost);
        }

        private void AddProducer(Entity entity, PositionInfo positionInfo)
        {
            var producerBuffer = entityManager.GetBuffer<Producer>(entity);
            var producers = buildings[currentBuilding].BuildingConfig.Producer;

            for (int i = 0; i < producers.Count; i++)
            {
                producerBuffer.Add(new Producer
                {
                    product = producers[i].product,
                    productionRate = BuildingUtils.GetRate(producers[i], positionInfo.moisturePercent, positionInfo.heightPercent)
                });
            }
        }

        private void AddConsumer(Entity entity, PositionInfo positionInfo)
        {
            var consumerBuffer = entityManager.GetBuffer<Consumer>(entity);
            var consumers = buildings[currentBuilding].BuildingConfig.Consumer;

            for (int i = 0; i < consumers.Count; i++)
            {
                consumerBuffer.Add(new Consumer
                {
                    product = consumers[i].product,
                    consumptionRate = BuildingUtils.GetRate(consumers[i], positionInfo.moisturePercent, positionInfo.heightPercent)
                });
            }
        }

        private void AddStorage(Entity entity)
        {
            var buildingConfig = buildings[currentBuilding].BuildingConfig;
            var allProducts = buildingConfig.Producer.Select(p => p.product).ToList();
            allProducts.AddRange(buildingConfig.Consumer.Select(p => p.product));
            allProducts.AddRange(buildingConfig.DefaultStorage.Select(p => p.product));
            allProducts = allProducts.Distinct().ToList();

            var storageBuffer = entityManager.GetBuffer<Storage>(entity);

            foreach (var product in allProducts)
            {
                var storedAmount = GetDefaultStorageAmount(product);
                storageBuffer.Add(new Storage
                {
                    product = product,
                    amount = storedAmount
                });
            }
        }

        private int GetDefaultStorageAmount(Product product)
        {
            var defautlStorage = buildings[currentBuilding].BuildingConfig.DefaultStorage.Find(stored => stored.product == product);
            return defautlStorage?.defaultAmount ?? 0;
        }

        private void SetBuildingModeData(bool isActive)
        {
            if (infoQuery.IsEmpty)
            {
                return;
            }

            var info = infoQuery.GetSingletonRW<BuildingModePositionInfo>();
            info.ValueRW.isInBuildingMode = isActive;

            if (!isActive)
            {
                return;
            }

            var infoEntity = infoQuery.GetSingletonEntity();
            entityManager.SetComponentData(infoEntity, new BuildingModeData
            {
                buildingData = buildings[currentBuilding]
            });

            var costInfo = infoQuery.GetSingletonRW<BuildingCostInfo>();
            costInfo.ValueRW.value = buildings[currentBuilding].CurrencyCost;
        }

        public override string GetBuildingName(int idx) => buildings[idx].BuildingName;

        protected override void OnActiveBuildingSelected()
        {
            
        }

        private bool DoesBuildingExistsOnPosition()
        {
            if (infoQuery.IsEmpty)
            {
                return false;
            }

            var info = infoQuery.GetSingleton<PositionInfo>();
            return info.hasBuilding;
        }

        private void TriggerOwnerBoudsRebuild(float3 pointerPosition)
        {
            if (infoQuery.IsEmpty)
            {
                return;
            }

            var config = infoQuery.GetSingletonRW<RecalculateOwnerBounds>();
            config.ValueRW.recalculate = true;

            var positions = infoQuery.GetSingletonBuffer<RecalculateOwneerBoundsPosition>();
            positions.Add(new RecalculateOwneerBoundsPosition
            {
                position = pointerPosition,
            });
        }
    }
}