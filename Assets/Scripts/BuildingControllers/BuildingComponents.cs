﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Assets.Scripts.BuildingControllers
{
    public enum Product
    {
        food,
        wood,
        plank,
        wheat,
        flour,
        bread,
        clay,
        brick,
        coal,
        ironOre,
        ironIngot,
        sword,
        unit,


        defaultProduct = 99999
    }

    public enum Owner
    {
        Owner1,
        Owner2,
        Owner3,
        Owner4,
        Owner5,
        Owner6,
    }

    public struct IntrestPoint : IComponentData
    {
        public float3 position;
        public FixedString32Bytes name;
        public Owner owner;
    }


    public struct Road : IComponentData
    {
    }

    public struct Building : IComponentData
    {

    }

    public struct UnitProducer : IComponentData
    {
    } 

    public struct Storage : IBufferElementData
    {
        public Product product;
        public int amount;
    }

    public struct Consumer : IBufferElementData
    {
        public Product product;
        public float consumptionRate;
        public float consumptionProgress;
    }


    public struct Producer : IBufferElementData
    {
        public Product product;
        public float productionRate;
        public float prodctionProgress;
    }

    public struct ConnectedIntrestPoint : IBufferElementData
    {
        public Entity entity;
    }
}